//
//  MainMenuView.h
//  GevorkGevorkyan
//
//  Created by Garnik Ghazaryan on 10/18/14.
//  Copyright (c) 2014 Garnik Ghazaryan. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MainMenuViewDelegate <NSObject>

- (void) didCatalogPressed;
- (void) didMenuPressed;
- (void) didCreatePressed;

@end

@interface MainMenuView : UIView

@property (nonatomic) id delegate;

@end
