//
//  YourDataViewController.m
//  GevorkGevorkyan
//
//  Created by Garnik Ghazaryan on 11/27/14.
//  Copyright (c) 2014 Garnik Ghazaryan. All rights reserved.
//

#import "YourDataViewController.h"
#import "Utils/Utils.h"
#import "CheckOutViewController.h"

@interface YourDataViewController () <UITextFieldDelegate,UITextViewDelegate>



@end

@implementation YourDataViewController{

    UIButton *confirmSizes;
    UIButton *ourLocation;
    UIButton *questionMarkButton;
    
    UIImageView *dotImage;
    
    UILabel *selfService;
    UILabel *delivery;
    UILabel *firstNameLabel;
    UILabel *lastNameLabel;
    UILabel *phoneNumberLabel;
    UILabel *emailLabel;
    UILabel *commentsLabel;
    UILabel *addressLabel;
    
    UISwitch *serviceSwitch;
    
    UITextField *firstNameField;
    UITextField *lastNameField;
    UITextField *phoneNumberField;
    UITextField *emailField;
    UITextField *addressField;
    
    UITextView *commentaryView;
    UIScrollView *bgScrollView;
    UIImageView *hintImageView;
    
    CGSize screenSize;
    CGFloat currentOffset;
    
    CheckOutViewController *checkOutViewController;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    self.view.backgroundColor = rgba(250, 249, 245, 1);
    self.title = @"ВАШИ ДАННЫЕ";
    self.navigationController.navigationBar.backgroundColor = [UIColor whiteColor];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithTitle:@"НАЗАД" style:UIBarButtonItemStyleBordered target:self action:@selector(goBack)];
    [leftItem  setTitleTextAttributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:14] } forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithTitle:@"ГОТОВО" style:UIBarButtonItemStyleBordered target:self action:@selector(finishEditing)];
    [rightItem  setTitleTextAttributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:14.0] } forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = rightItem;
    self.navigationController.navigationBar.tintColor = [UIColor grayColor];
    
    screenSize = [Utils getScreenBounds];
    [self uiConfigurations];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) uiConfigurations{
    
    
    bgScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 44, screenSize.width, screenSize.height)];
    bgScrollView.contentSize = CGSizeMake(screenSize.width, 600);
    bgScrollView.bounces = NO;
    
    [Utils setViewToAddOn:bgScrollView];
    confirmSizes = [Utils createButtonInRect:CGRectMake(10, -34, screenSize.width - 20, 40) title:@"" target:self action:@selector(sizesConfirmed) backGroundImage:[UIImage imageNamed:@"sizesEntererdButtonImage.png"] highlightImage:nil];
    dotImage = [Utils createImageViewWithRect:CGRectMake(5, confirmSizes.frame.origin.y + confirmSizes.frame.size.height + 10, screenSize.width - 10, 2) image:[UIImage imageNamed:@"dotImage.png"]];
    serviceSwitch = [Utils createSwitchWithRect:CGRectMake(screenSize.width/2 - 30, dotImage.frame.origin.y + 17, 60, 40) target:self action:@selector(serviceSwitchChange)];
    selfService = [Utils createLabelWithRect:CGRectMake(serviceSwitch.frame.origin.x - 110, serviceSwitch.frame.origin.y, 100, 30) title:@"САМОВЫЗОВ"];
    delivery = [Utils createLabelWithRect:CGRectMake(serviceSwitch.frame.origin.x + serviceSwitch.frame.size.width + 10, serviceSwitch.frame.origin.y, 100, 30) title:@"ДОСТАВКА"];
    questionMarkButton = [Utils createButtonInRect:CGRectMake(screenSize.width - 40, delivery.frame.origin.y, 30, 30) title:@"" target:self action:@selector(showHint) backGroundImage:[UIImage imageNamed:@"questionMarkImage.png"] highlightImage:nil];
    
    firstNameLabel = [Utils createLabelWithRect:CGRectMake(10, selfService.frame.origin.y + selfService.frame.size.height + 15, 140, 30) title:@"ИМЯ:"];
    lastNameLabel = [Utils createLabelWithRect:CGRectMake(screenSize.width/2 + 5, selfService.frame.origin.y + selfService.frame.size.height + 15, 140, 30) title:@"ФАМИЛИЯ:"];
    firstNameField = [Utils createTextFieldWithRect:CGRectMake(10, firstNameLabel.frame.origin.y + firstNameLabel.frame.size.height + 3, screenSize.width/2 - 15, 40) title:@"" placeHolderText:@"ИМЯ"];
    lastNameField = [Utils createTextFieldWithRect:CGRectMake(screenSize.width/2 + 5, firstNameLabel.frame.origin.y + firstNameLabel.frame.size.height + 3, screenSize.width/2 - 15, 40) title:@"" placeHolderText:@"ФАМИЛИЯ"];
    phoneNumberLabel = [Utils createLabelWithRect:CGRectMake(10, firstNameField.frame.origin.y + firstNameField.frame.size.height + 15, screenSize.width - 20, 30) title:@"ТЕЛЕФОН:"];
    phoneNumberField = [Utils createTextFieldWithRect:CGRectMake(10, phoneNumberLabel.frame.origin.y + phoneNumberLabel.frame.size.height + 3, screenSize.width - 20, 40) title:@"" placeHolderText:@"+7 (987) 654-32-10"];

    emailLabel = [Utils createLabelWithRect:CGRectMake(10, phoneNumberField.frame.origin.y + phoneNumberField.frame.size.height + 15, screenSize.width - 20, 30) title:@"EMAIL:"];
    emailField = [Utils createTextFieldWithRect:CGRectMake(10, emailLabel.frame.origin.y + emailLabel.frame.size.height + 3, screenSize.width - 20, 40) title:@"" placeHolderText:@"show@boots.com"];

    addressLabel = [Utils createLabelWithRect:CGRectMake(10, emailField.frame.origin.y + emailField.frame.size.height + 15, screenSize.width - 20, 30) title:@"АДРЕС:"];
    addressField = [Utils createTextFieldWithRect:CGRectMake(10, addressLabel.frame.origin.y + addressLabel.frame.size.height + 3, screenSize.width - 20, 40) title:@"" placeHolderText:@"ул. Подкаблучная, д.23, кв. 42"];

    ourLocation = [Utils createButtonInRect:CGRectMake(10, addressField.frame.origin.y - 15, screenSize.width - 20, 40) title:@"" target:self action:@selector(showOurLocation) backGroundImage:[UIImage imageNamed:@"ourLocationButtonImage.png"] highlightImage:nil];
    commentsLabel = [Utils createLabelWithRect:CGRectMake(10, addressField.frame.origin.y + addressField.frame.size.height + 10 , 150, 30) title:@"КОММЕНТАРИЙ:"];
    commentaryView = [Utils createTextViewWithRect:CGRectMake(10, commentsLabel.frame.origin.y + 40, screenSize.width - 20, 70) contentText:@"Эти туфли мне на свадьбу. Постарайтесь над ними. Я хочу сразить! :)"];
    
    hintImageView = [Utils createImageViewWithRect:CGRectMake(0, 60, 183, 50) image:[UIImage imageNamed:@"deliveryHint.png"]];
    hintImageView.hidden = YES;
    
    selfService.font = [UIFont systemFontOfSize:14.0];
    delivery.font = [UIFont systemFontOfSize:14.0];
    selfService.textColor = rgba(231, 198, 128, 1);
    
    firstNameField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    lastNameField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    phoneNumberField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    emailField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    
    firstNameField.delegate = self;
    lastNameField.delegate = self;
    phoneNumberField.delegate = self;
    emailField.delegate = self;
    addressField.delegate = self;
    commentaryView.delegate = self;

    firstNameField.background           = [UIImage imageNamed:@"textFieldBg.png"];
    lastNameField.background            = [UIImage imageNamed:@"textFieldBg.png"];
    phoneNumberField.background         = [UIImage imageNamed:@"textFieldBg.png"];
    emailField.background               = [UIImage imageNamed:@"textFieldBg.png"];
    addressField.background             = [UIImage imageNamed:@"textFieldBg.png"];
    

    addressField.hidden = YES;
    addressField.enabled = NO;
    addressLabel.hidden = YES;
    
    [bgScrollView addSubview:serviceSwitch];
    [self.view addSubview:bgScrollView];
}
#pragma mark Actions

- (void) sizesConfirmed{
    
    
}
- (void) showOurLocation{
    
    
}
- (void) serviceSwitchChange{
    
    if (!serviceSwitch.on) {
        addressField.hidden = YES;
        addressField.enabled = NO;
        addressLabel.hidden = YES;
        
        ourLocation.hidden = NO;
        ourLocation.enabled = YES;
        

        selfService.textColor = rgba(231, 198, 128, 1);
        delivery.textColor = rgba(153, 153, 153, 1);
        commentaryView.text = @"Эти туфли мне на свадьбу. Постарайтесь над ними. Я хочу сразить! :)";
        
    }else{
        addressField.hidden = NO;
        addressField.enabled = YES;
        addressLabel.hidden = NO;
        
        ourLocation.hidden = YES;
        ourLocation.enabled = NO;

        commentaryView.text = @"Код домофона: #3565\nЖелаемое время доставки: с 14:00 - 19:00\nДоп. телефон: +7 (012) 345-67-89";
        selfService.textColor = rgba(153, 153, 153, 1);
        delivery.textColor = rgba(231, 198, 128, 1);

    }
    
}
- (void) finishEditing{
    checkOutViewController = [[CheckOutViewController alloc] init];
    [self.navigationController pushViewController:checkOutViewController animated:YES];
    
}
- (void) goBack{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void) showHint{
    
    CGRect tmpRect = hintImageView.frame;
    tmpRect.origin = CGPointMake((questionMarkButton.frame.origin.x - tmpRect.size.width) + questionMarkButton.frame.size.width/2 + 15, questionMarkButton.frame.origin.y + questionMarkButton.frame.size.height + 5);
    hintImageView.frame = tmpRect;
    hintImageView.hidden = NO;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(onSingleTap:)];
    tap.cancelsTouchesInView = NO;
    [bgScrollView addGestureRecognizer:tap];
    
    for (NSString *familyName in [UIFont familyNames]) {
        for (NSString *fontName in [UIFont fontNamesForFamilyName:familyName]) {
            NSLog(@"%@", fontName);
        }
    }
}
- (void)onSingleTap:(UITapGestureRecognizer *)sender
{
    [hintImageView setHidden:YES];
}
#pragma mark Other Methods
- (void)keyboardWillShow:(NSNotification *)notification andView:(UIView*) view
{
    CGRect tmpRect = [view convertRect:view.bounds toView:self.view];
    currentOffset = bgScrollView.contentOffset.y;
    
    [UIView animateWithDuration:0.3 animations:^{
        if (tmpRect.origin.y > self.view.frame.size.height - 250) {
            [bgScrollView setContentOffset:CGPointMake(0, currentOffset + (tmpRect.origin.y - 264) + tmpRect.size.height + 15) animated:YES];
        }
    }];
}

-(void)keyboardWillHide:(NSNotification *)notification andView:(UIView*) view
{
    [UIView animateWithDuration:0.3 animations:^{
        [bgScrollView setContentOffset:CGPointMake(0, currentOffset) animated:YES];
        [bgScrollView setContentSize:CGSizeMake(screenSize.width, 600)];
    }];
}
#pragma mark TextField and TextView Delegates

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    [self keyboardWillShow:nil andView:textField];
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    [self keyboardWillHide:nil andView:textField];
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    [self keyboardWillShow:nil andView:textView];
}
-(void)textViewDidEndEditing:(UITextView *)textView{
    [self keyboardWillHide:nil andView:textView];
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
