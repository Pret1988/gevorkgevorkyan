//
//  PlatformHeightAdjustView.m
//  GevorkGevorkyan
//
//  Created by Garnik Ghazaryan on 11/21/14.
//  Copyright (c) 2014 Garnik Ghazaryan. All rights reserved.
//

#import "PlatformHeightAdjustView.h"
#import "Utils.h"
@implementation PlatformHeightAdjustView{
    
    UISlider *higherSlider;
    UISlider *lowerSlider;
    
    UILabel *higherSliderLabel;
    UILabel *lowerSliderLabel;
    
    UIImageView *heelImageView;
    UIImageView *higherSliderBGView;
    UIImageView *lowerSliderBGView;
    
    CGSize viewSize;
    NSMutableAttributedString *labelString;
    int higherLabelNumber;
    int lowerLabelNumber;
}
@synthesize isSwitchOn;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        viewSize = frame.size;
        [self uiConfigurations];
    }
    return self;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


- (void) uiConfigurations{
    
    [Utils setViewToAddOn:self];
    self.backgroundColor = [UIColor clearColor];
    
    higherSliderBGView = [Utils createImageViewWithRect:CGRectMake(30, viewSize.height - 100, 260, 6) image:[UIImage imageNamed:@"sliderDottedImage.png"]];
    lowerSliderBGView = [Utils createImageViewWithRect:CGRectMake(30, higherSliderBGView.frame.origin.y + 68, 260, 6) image:[UIImage imageNamed:@"sliderDottedImage.png"]];
    
    higherSlider = [[UISlider alloc] initWithFrame:CGRectMake(higherSliderBGView.frame.origin.x - 15, higherSliderBGView.frame.origin.y - 15, higherSliderBGView.frame.size.width + 30, 35)];
    [higherSlider addTarget:self action:@selector(higherSliderMoved) forControlEvents:UIControlEventValueChanged];
    
    lowerSlider = [[UISlider alloc] initWithFrame:CGRectMake(lowerSliderBGView.frame.origin.x - 15, lowerSliderBGView.frame.origin.y - 15, lowerSliderBGView.frame.size.width + 30, 35)];
    [lowerSlider addTarget:self action:@selector(lowerSliderMoved) forControlEvents:UIControlEventValueChanged];
    
//    higherSlider.backgroundColor = rgba(0.5, 0.4, 0.2, 0.3);
//    lowerSlider.backgroundColor = rgba(0.5, 0.4, 0.2, 0.3);
    higherSlider.continuous = YES;
    lowerSlider.continuous = YES;
    
    [self setupAppearance:higherSlider];
    [self setupAppearance:lowerSlider];
    
    higherLabelNumber = 4;
    lowerLabelNumber = 0;
    
    CGRect tmpRect = higherSliderBGView.frame;
    
    tmpRect.origin.y -= 45;
    tmpRect.size.height = 30;
    higherSliderLabel = [Utils createLabelWithRect:tmpRect title:[NSString stringWithFormat:@"Высота каблука: %d см",higherLabelNumber]];
    
    tmpRect = lowerSliderBGView.frame;
    tmpRect.origin.y -= 45;
    tmpRect.size.height = 30;
    lowerSliderLabel = [Utils createLabelWithRect:tmpRect title:[NSString stringWithFormat:@"Высота платформы: %d см",lowerLabelNumber]];

    
    higherSlider.value = 4.0;
    lowerSlider.value = 0.0;
    
    

    
    heelImageView = [[UIImageView alloc] initWithFrame:CGRectMake(viewSize.width/2 - 95, 10, 193, 174)];
    heelImageView.contentMode = UIViewContentModeScaleAspectFit;
    
    [self changeLabelsAndImageNameWithNumber:lowerSlider.value];
    
    
    [self addSubview:higherSlider];
    [self addSubview:lowerSlider];
    [self addSubview:heelImageView];
    
}

-(void)setupAppearance:(UISlider*)slider {
    
    [slider setMaximumTrackTintColor:rgba(1, 1, 1, 0)];
    [slider setMinimumTrackTintColor:rgba(1, 1, 1, 0)];

    slider.maximumValue = 9;
    slider.minimumValue = 0;

}

#pragma mark Actions
- (void) higherSliderMoved{
    NSUInteger index = (NSUInteger)(higherSlider.value + 0.5);
    [higherSlider setValue:index animated:NO];
    if (higherSlider.value >= 4.0 ) {
        lowerSlider.value = higherSlider.value - 4.0;
    }else {
//        higherSlider.value = 4.0;
        lowerSlider.value = 0.0;
    }
    [self changeLabelsAndImageNameWithNumber:lowerSlider.value];
}
- (void) lowerSliderMoved{
    NSUInteger index = (NSUInteger)(lowerSlider.value + 0.5);
    [lowerSlider setValue:index animated:NO];
    if (lowerSlider.value <= 6.0 ) {
        higherSlider.value = lowerSlider.value + 4.0;
    }else {
//        lowerSlider.value = 6.0;
        higherSlider.value = 9.0;
    }
    [self changeLabelsAndImageNameWithNumber:lowerSlider.value];
}
- (void) changeLabelsAndImageNameWithNumber:(int)nameNumber{
    higherLabelNumber = higherSlider.value;
    lowerLabelNumber = lowerSlider.value;
    higherSliderLabel.text = [NSString stringWithFormat:@"Высота каблука: %d см",higherLabelNumber];
    lowerSliderLabel.text = [NSString stringWithFormat:@"Высота платформы: %d см",lowerLabelNumber];
    NSString *heelImageName = [self setImageNameWithNumber:nameNumber];
    if ([UIImage imageNamed:heelImageName]) {
        heelImageView.image = [UIImage imageNamed:heelImageName];
    }

}
-(void)setIsSwitchOn:(BOOL)isSwitchOn1{
    if (isSwitchOn1 != isSwitchOn) {
        isSwitchOn = isSwitchOn1;
    }
    [self changeLabelsAndImageNameWithNumber:lowerSlider.value];
}
- (NSString*)setImageNameWithNumber:(int)imageNumber{
    
    NSString *imageName = nil;
    if (isSwitchOn) {
        imageName = [NSString stringWithFormat:@"platformShoeRaw%d.png",imageNumber];
    }else{
        imageName = [NSString stringWithFormat:@"heelShoeRaw%d.png",imageNumber];
    }
    return imageName;
}
#pragma Other Methods

/*
 UIImage *minImage = [[UIImage imageNamed:@"platformSliderMinimum.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
 UIImage *maxImage = [[UIImage imageNamed:@"platformSliderMaximum.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 5)];

 
 //    [slider setMaximumTrackImage:maxImage forState:UIControlStateNormal];
 //    [slider setMinimumTrackImage:minImage forState:UIControlStateNormal];
 */

@end
