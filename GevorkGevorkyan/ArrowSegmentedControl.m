//
//  ArrowSegmentedControl.m
//  GevorkGevorkyan
//
//  Created by Garnik Ghazaryan on 10/31/14.
//  Copyright (c) 2014 Garnik Ghazaryan. All rights reserved.
//

#import "ArrowSegmentedControl.h"

@implementation ArrowSegmentedControl

- (id)initWithItems:(NSArray *)items {
    self = [super initWithItems:items];
    if (self) {
        // Initialization code
        
//         Set divider images
        [self setDividerImage:[UIImage imageNamed:@"segContrNoneSel.png"]
          forLeftSegmentState:UIControlStateNormal
            rightSegmentState:UIControlStateNormal
                   barMetrics:UIBarMetricsDefault];
        [self setDividerImage:[UIImage imageNamed:@"segContrLeftSel.png"]
          forLeftSegmentState:UIControlStateSelected
            rightSegmentState:UIControlStateNormal
                   barMetrics:UIBarMetricsDefault];
        [self setDividerImage:[UIImage imageNamed:@"segContrRightSel.png"]
          forLeftSegmentState:UIControlStateNormal
            rightSegmentState:UIControlStateSelected
                   barMetrics:UIBarMetricsDefault];
//        [self setDividerImage:[UIImage imageNamed:@"segContrHighlightBG.png"]
//          forLeftSegmentState:UIControlStateHighlighted
//            rightSegmentState:UIControlStateHighlighted
//                   barMetrics:UIBarMetricsDefault];
        
        // Set background images
        UIImage *normalBackgroundImage = [UIImage imageNamed:@"segContrNotSelBg.png"];
        [self setBackgroundImage:normalBackgroundImage
                        forState:UIControlStateNormal
                      barMetrics:UIBarMetricsDefault];
        UIImage *selectedBackgroundImage = [UIImage imageNamed:@"segContr.png"];
        [self setBackgroundImage:selectedBackgroundImage
                        forState:UIControlStateSelected
                      barMetrics:UIBarMetricsDefault];
        [self setBackgroundImage:[UIImage imageNamed:@"segContrHighlightBG.png"]
                        forState:UIControlStateHighlighted
                      barMetrics:UIBarMetricsDefault];
        
        [self setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                 [UIColor colorWithRed:78.0/255.0 green:60.0/255.0 blue:54.0/255.0 alpha:1.0],NSForegroundColorAttributeName,
//
                                                                 [UIFont fontWithName:@"HelveticaNeue-Bold" size:10.0], NSFontAttributeName, nil] forState:UIControlStateNormal];
        
        [self setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                 [UIColor whiteColor],NSForegroundColorAttributeName,
                                                                 [UIFont fontWithName:@"HelveticaNeue-Bold" size:10.0], NSFontAttributeName, nil] forState:UIControlStateSelected];
        
        float dividerImageWidth = [UIImage imageNamed:@"segContrRightSel.png"].size.width;
        [self setContentPositionAdjustment:UIOffsetMake(dividerImageWidth / 2, 0)
                            forSegmentType:UISegmentedControlSegmentLeft
                                barMetrics:UIBarMetricsDefault];
        [self setContentPositionAdjustment:UIOffsetMake(- dividerImageWidth / 2, 0)
                            forSegmentType:UISegmentedControlSegmentRight
                                barMetrics:UIBarMetricsDefault];
        
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
