//
//  KingBirdStudioViewController.m
//  GevorkGevorkyan
//
//  Created by Garnik Ghazaryan on 12/3/14.
//  Copyright (c) 2014 Garnik Ghazaryan. All rights reserved.
//

#import "KingBirdStudioViewController.h"
#import "Utils.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h> 

@interface KingBirdStudioViewController ()<MFMailComposeViewControllerDelegate>

@end

@implementation KingBirdStudioViewController{

    CGSize  screenSize;
    
    UIImageView *bgImage;
    UIButton    *openUrl;
    UIButton    *emailButton;

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    self.view.backgroundColor = rgba(245, 243, 236, 1);
    self.title = @"МОИ ЭСКИЗЫ";
    self.navigationController.navigationBar.backgroundColor = [UIColor whiteColor];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithTitle:@"МЕНЮ" style:UIBarButtonItemStyleBordered target:self action:@selector(goBack)];
    [leftItem  setTitleTextAttributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:14]} forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem = leftItem;
    // Do any additional setup after loading the view.
    self.navigationController.navigationBar.tintColor = [UIColor grayColor];
    screenSize = [Utils getScreenBounds];
    [self uiConfigurations];
}
- (void) uiConfigurations{
    
  
    bgImage = [Utils createImageViewWithRect:self.view.frame image:[UIImage imageNamed:@"kingBirdStudioBGImage.png"]];
    openUrl = [Utils createButtonInRect:CGRectMake(screenSize.width/2 - 90, 200, 180, 18) title:@"" target:self action:@selector(openKingbirdURL) backGroundImage:[UIImage imageNamed:@"kingbirdwebsite.png"] highlightImage:nil];
    emailButton = [Utils createButtonInRect:CGRectMake(screenSize.width/2 - 69 , screenSize.height - 60, 138, 37) title:@"" target:self action:@selector(composeEmail) backGroundImage:[UIImage imageNamed:@"emailUs.png"] highlightImage:nil];
    
}

#pragma mark Actions

- (void) goBack{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void) openKingbirdURL{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.kingbird.ru"]];
}
- (void) composeEmail{
 
    // From within your active view controller
    if([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailCont = [[MFMailComposeViewController alloc] init];
        mailCont.mailComposeDelegate = self;        // Required to invoke mailComposeController when send
        
        [mailCont setSubject:@"Email subject"];
        [mailCont setToRecipients:[NSArray arrayWithObject:@"myFriends@email.com"]];
        [mailCont setMessageBody:@"Email message" isHTML:NO];
        
        [self presentViewController:mailCont animated:YES completion:nil];
    }

    
}
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [controller dismissViewControllerAnimated:YES completion:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
