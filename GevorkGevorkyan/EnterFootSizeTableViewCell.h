//
//  EnterFootSizeTableViewCell.h
//  GevorkGevorkyan
//
//  Created by Garnik Ghazaryan on 12/5/14.
//  Copyright (c) 2014 Garnik Ghazaryan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EnterFootSizeTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *sizeLabel;

@end
