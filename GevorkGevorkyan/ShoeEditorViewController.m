//
//  ShoeEditorViewController.m
//  GevorkGevorkyan
//
//  Created by Garnik Ghazaryan on 10/31/14.
//  Copyright (c) 2014 Garnik Ghazaryan. All rights reserved.
//

#import "ShoeEditorViewController.h"
#import "Utils.h"
#import "ArrowSegmentedControl.h"
#import "ColorPickerView.h"
#import "AKPickerView.h"
#import "BeltDetailViewController.h"
#import "MZFormSheetController.h"
#import "ShoeFormDetailViewController.h"
#import "YourDataViewController.h"
#import "ChangeImageColor.h"

@interface ShoeEditorViewController () <ColorPickerViewDelegate,AKPickerViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) AKPickerView *pickerView;
@property (nonatomic, strong) NSArray *filterTitles;


@end

@implementation ShoeEditorViewController{
    
    CGSize screenSize;
    BOOL isInResultView;
    
    UIButton *createNewShoeButton;
    ArrowSegmentedControl *segControl;
    UIView *shoeEditorView;
    CAShapeLayer *shapeLayer;
    CAShapeLayer *shapeLayer1;
    ColorPickerView *colorPickerView;
    UIView *editorFilterView;
    UIImageView *resultFloatingView;
    UIView *draggerView;
    UILabel *totalSum;
    UIView *beltCollectionView;
    UICollectionView *_collectionView;
    BeltDetailViewController *beltViewController;
    YourDataViewController *yourDataViewController;
    
    UIImageView *shoeHeelImageView;
    UIImageView *shoeInsideImageView;
    UIImageView *shoeLeftAndNose;
    UIImageView *shoePlatformView;
    UIImageView *shoeRightImageView;
    
    UIImageView *colorChangeImageView;
    
    UIButton *changePlatformHeight;
    UIButton *changeNoseMaterial;
    
    UIButton *setShoeInsideColor;
    UIButton *setShoeLeftAndNoseColor;
    UIButton *setPlatformColor;
    UIButton *setRightColor;

    BOOL editorViewSlideIsUp;
    BOOL beltViewSlideIsLeft;
    BOOL isToShowMaterials;

    UIColor *selectedColor;
    UICollectionViewCell *someCell;
    UIColor *currentColor;
}
- (id)init
{
    self = [super init];
    if (self) {
    }
    return self;
}
- (void)viewDidLoad {
    
    
    self.view = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    self.view.backgroundColor = rgba(250, 249, 245, 1);
    self.title = @"РЕДАКТОР";
    self.navigationController.navigationBar.backgroundColor = [UIColor whiteColor];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithTitle:@"ОТМЕНА" style:UIBarButtonItemStyleBordered target:self action:@selector(goBack)];
    [leftItem  setTitleTextAttributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:14] } forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithTitle:@"ГОТОВО" style:UIBarButtonItemStyleBordered target:self action:@selector(finishEditing)];
    [rightItem  setTitleTextAttributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:14.0] } forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = rightItem;
    self.navigationController.navigationBar.tintColor = [UIColor grayColor];
    
    screenSize = [Utils getScreenBounds];// Do any additional setup after loading the view.
    

    
    [self uiConfigurations];
    [self configureColorPickerView];
    [self configurePickerView];
    [self drawShoePart1];
    [self drawShoePart2];
    [self setupTouches];
    
    [editorFilterView addSubview:colorPickerView];
    [editorFilterView addSubview:self.pickerView];
    
    yourDataViewController = [[YourDataViewController alloc] init];
    
    [super viewDidLoad];
}
-(void)viewDidAppear:(BOOL)animated {

    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void) configureColorPickerView {
    
    colorPickerView = [[ColorPickerView alloc] initWithFrame:CGRectMake(0, editorFilterView.frame.size.height/2 , screenSize.width, 50)];
    [colorPickerView enableHorizontalScroll];
    [colorPickerView setIsMultipleSelectionAllowed:NO];
    colorPickerView.delegate = self;
    colorPickerView.colorItems = [[NSMutableArray alloc] initWithObjects:
                                  [UIColor redColor],
                                  [UIColor yellowColor],
                                  [UIColor greenColor],
                                  [UIColor blueColor],
                                  [UIColor grayColor],
                                  [UIColor lightGrayColor],
                                  [UIColor blackColor],
                                  [UIColor yellowColor],
                                  [UIColor greenColor],
                                  [UIColor blueColor],
                                  [UIColor greenColor],
                                  [UIColor blackColor],
                                  nil];
    colorPickerView.hidden = NO;
    [colorPickerView viewInitiated];
}
- (void) configurePickerView {
    
    self.pickerView = [[AKPickerView alloc] initWithFrame:CGRectMake(-100, 0, screenSize.width + 200, 50)];
    self.pickerView.delegate = self;
    self.pickerView.highlightedTextColor = rgba(253, 198, 128, 1);
    self.pickerView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.pickerView.backgroundColor = rgba(250, 250, 245, 1);
    self.pickerView.layer.borderWidth = 1.f;
    self.pickerView.layer.borderColor = rgba(149, 149, 149, 1).CGColor;
    [self.pickerView selectItem:2 animated:NO];

    UIImageView *dotImage = [[UIImageView alloc] initWithFrame:CGRectMake(self.pickerView.frame.size.width/2 - 2, 5, 6, 6)];
    dotImage.image = [UIImage imageNamed:@"selectedDot.png"];
    [self.pickerView addSubview:dotImage];
    self.pickerView.font = [UIFont systemFontOfSize:16.0];
    self.pickerView.highlightedFont = [UIFont systemFontOfSize:18.0];
    self.pickerView.interitemSpacing = 20.0;
    self.pickerView.fisheyeFactor = 0;
    
    self.filterTitles = @[@"САПОГИ",
                          @"ТУФЛИ",
                          @"НОВИНКИ",
                          @"БАЛЕТКИ",
                          @"БОТИНКИ",
                          @"Chiba",
                          @"Hyogo",
                          @"Hokkaido",
                          @"Fukuoka",
                          @"Shizuoka"];
    
    [self.pickerView reloadData];
    
}
- (void) configureCollectionView{
    
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    _collectionView=[[UICollectionView alloc] initWithFrame:beltCollectionView.bounds collectionViewLayout:layout];
    [_collectionView setDataSource:self];
    [_collectionView setDelegate:self];
    [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    _collectionView.backgroundColor = rgba(247, 245, 239, 1);
    [beltCollectionView addSubview:_collectionView];
    
}

- (void) uiConfigurations{

    segControl = [[ArrowSegmentedControl alloc] initWithItems:@[@"ФОРМА",@"МАТЕРИАЛ",@"АКСЕССУАРЫ"]];
    segControl.frame = CGRectMake(5, 50, screenSize.width - 10, 32);
    segControl.selectedSegmentIndex = 0;
    [segControl addTarget:self action:@selector(segmentedControlValueChanged:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:segControl];
    
    shoeEditorView = [[UIView alloc] initWithFrame:CGRectMake(0, segControl.frame.origin.x + segControl.frame.size.height + 50, screenSize.width, 300)];
    shoeEditorView.backgroundColor = self.view.backgroundColor;
    [self.view addSubview:shoeEditorView];
    
    editorFilterView = [[UIView alloc] initWithFrame:CGRectMake(0, screenSize.height, screenSize.width, 100)];
    editorFilterView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:editorFilterView];
    
    resultFloatingView = [[UIImageView alloc] initWithFrame:CGRectMake(0, screenSize.height - 50, screenSize.width, 164)];
    resultFloatingView.image = [UIImage imageNamed:@"resultFloatingViewImage.png"];
    resultFloatingView.userInteractionEnabled = YES;

    draggerView = [[UIView alloc] initWithFrame:CGRectMake(resultFloatingView.frame.size.width/2 - 20, 0, 60, 60)];
    draggerView.backgroundColor = [UIColor clearColor];
    [resultFloatingView addSubview:draggerView];
    totalSum = [Utils createLabelWithRect:CGRectMake(5, 10, resultFloatingView.frame.size.width - 10, 50) title:@"ИТОГОВАЯ СУММА:          13 100 Р"];
    [resultFloatingView addSubview:totalSum];
    
    [Utils setViewToAddOn:shoeEditorView];
    int x = 108;
    int y = 228;
    int width = 130;
    int height = 55;
    
    shoePlatformView = [Utils createImageViewWithRect:CGRectMake(x, y, width, height) image:[UIImage imageNamed:@"rawShoePlatform.png"]];
    shoeInsideImageView = [Utils createImageViewWithRect:CGRectMake(x - 12, y - 121, width - 54, height + 58) image:[UIImage imageNamed:@"rawShoeInside.png"]];
    shoeHeelImageView = [Utils createImageViewWithRect:CGRectMake(x - 34, y - 95, width - 108, height + 51) image:[UIImage imageNamed:@"rawShoeHeel.png"]];
    shoeRightImageView = [Utils createImageViewWithRect:CGRectMake(x-22, y - 162, width - 7, height + 101) image:[UIImage imageNamed:@"rawShoeRight.png"]];
    shoeLeftAndNose = [Utils createImageViewWithRect:CGRectMake(x - 39, y - 162, width + 39, height + 140) image:[UIImage imageNamed:@"rawShoeLeftAndNose.png"]];
    
    isToShowMaterials = TRUE;
    selectedColor = [UIColor purpleColor];
    
    changePlatformHeight = [Utils createButtonInRect:CGRectMake(x - 39, y - 162, 32, 32) title:@"" target:self action:@selector(showPlatformAdjuster) backGroundImage:[UIImage imageNamed:@"changeHeighButton.png"] highlightImage:nil];
    changeNoseMaterial = [Utils createButtonInRect:CGRectMake(x + 105, y, 32, 32) title:@"" target:self action:@selector(showMaterials) backGroundImage:[UIImage imageNamed:@"changeHeighButton.png"] highlightImage:nil];
//    [shoeEditorView addSubview:changePlatformHeight];
    
    setShoeInsideColor = [Utils createButtonInRect:CGRectMake(x + 3, y - 80, 32, 32) title:@"" target:self action:@selector(setColorOfPartWithSender:) backGroundImage:[UIImage imageNamed:@"changeHeighButton.png"] highlightImage:nil];
    setShoeLeftAndNoseColor = [Utils createButtonInRect:CGRectMake(x - 39, y - 162, 32, 32) title:@"" target:self action:@selector(setColorOfPartWithSender:) backGroundImage:[UIImage imageNamed:@"changeHeighButton.png"] highlightImage:nil];
    setPlatformColor = [Utils createButtonInRect:CGRectMake(x + 105, y + 25, 32, 32) title:@"" target:self action:@selector(setColorOfPartWithSender:) backGroundImage:[UIImage imageNamed:@"changeHeighButton.png"] highlightImage:nil];
    setRightColor = [Utils createButtonInRect:CGRectMake(x + 20, y - 130, 32, 32) title:@"" target:self action:@selector(setColorOfPartWithSender:) backGroundImage:[UIImage imageNamed:@"changeHeighButton.png"] highlightImage:nil];
    
    setShoeInsideColor.hidden = YES;
    setShoeInsideColor.enabled = NO;
    setShoeLeftAndNoseColor.hidden = YES;
    setShoeLeftAndNoseColor.enabled = NO;
    setPlatformColor.hidden = YES;
    setPlatformColor.enabled = NO;
    setRightColor.hidden = YES;
    setRightColor.enabled = NO;
    
    setShoeInsideColor.tag = 1;
    setShoeLeftAndNoseColor.tag = 2;
    setPlatformColor.tag = 3;
    setRightColor.tag = 4;
    currentColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:1];
    
    beltCollectionView = [[UIView alloc] initWithFrame:CGRectMake(screenSize.width, segControl.frame.origin.y + segControl.frame.size.height + editorFilterView.frame.size.height + 10, screenSize.width, screenSize.height - (segControl.frame.origin.y + segControl.frame.size.height + editorFilterView.frame.size.height) - 50)];
    [self.view addSubview:beltCollectionView];
    
    [self.view bringSubviewToFront:editorFilterView];
    
    [self.view addSubview:resultFloatingView];
    [self configureCollectionView];
}
#pragma mark Collection view delegates;
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 15;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell* cell = (UICollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    cell.backgroundColor = [UIColor clearColor];
    UIImageView *itemView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    if (segControl.selectedSegmentIndex == 2) {
        itemView.image = [UIImage imageNamed:@"beltImage.png"];
        cell.selectedBackgroundView.backgroundColor = [UIColor clearColor];
        cell.contentView.backgroundColor = [UIColor clearColor];
        cell.tag = indexPath.row;

        [cell.contentView addSubview:itemView];
    }
    if (isToShowMaterials){
        cell.contentView.backgroundColor = [UIColor clearColor];
        CGColorRef myColorRef = selectedColor.CGColor;
        const CGFloat *colorComponents = CGColorGetComponents(myColorRef);
        CGFloat alpha = CGColorGetAlpha(selectedColor.CGColor);
        if (alpha > 0.2) {
            alpha -= 0.04 * indexPath.row;
        }
        if (CGColorGetNumberOfComponents(myColorRef) == 2) {
            cell.contentView.backgroundColor = [UIColor colorWithRed:colorComponents[0] green:colorComponents[0] blue:colorComponents[0] alpha:alpha];
        }
        else if (CGColorGetNumberOfComponents(myColorRef) == 4) {
//            NSLog(@"r=%f, g=%f, b=%f, a=%f", colorComponents[0], colorComponents[1], colorComponents[2], colorComponents[3]);
            cell.contentView.backgroundColor = [UIColor colorWithRed:colorComponents[0] green:colorComponents[1] blue:colorComponents[2] alpha:alpha];
        }
    }
  

    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(100, 110);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 2.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 2.0;
}

// Layout: Set Edges
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    // return UIEdgeInsetsMake(0,8,0,8);  // top, left, bottom, right
    return UIEdgeInsetsMake(0,5,0,5);  // top, left, bottom, right
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSLog(@"Selected items # %ld",(long)indexPath.row);
    UICollectionViewCell *selectedCell = [collectionView cellForItemAtIndexPath:indexPath];
    [selectedCell setSelected:YES];
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 100, 101)];
    imageView.image = [UIImage imageNamed:@"cellSelectedBack.png"];

    for (UIView *subView in selectedCell.contentView.subviews) {
        if (CGRectEqualToRect(subView.frame, CGRectMake(0, 0, 100, 101))) {
            [subView removeFromSuperview];
            
        }
        NSLog(@"subview %@",subView);
    }

    if (isToShowMaterials) {
        editorViewSlideIsUp = NO;
        someCell = selectedCell;
        [self showAccessories];
        
    }
    

    if (segControl.selectedSegmentIndex == 2) {
        
        [selectedCell.contentView addSubview:imageView];
        beltViewController = [[BeltDetailViewController alloc] init];
        beltViewController.preferredContentSize = CGSizeMake(300, 400);
        MZFormSheetController *formSheet = [[MZFormSheetController alloc] initWithViewController:beltViewController];
        
        formSheet.presentedFormSheetSize = CGSizeMake(300, 400);
        //    formSheet.transitionStyle = MZFormSheetTransitionStyleSlideFromTop;
        formSheet.shadowRadius = 2.0;
        formSheet.shadowOpacity = 0.3;
        formSheet.shouldDismissOnBackgroundViewTap = YES;
        formSheet.shouldCenterVertically = YES;
        formSheet.movementWhenKeyboardAppears = MZFormSheetWhenKeyboardAppearsCenterVertically;
        
        [self mz_presentFormSheetController:formSheet animated:YES completionHandler:nil];
    
        
    }else if (segControl.selectedSegmentIndex == 1){
        UIImage *newImage = [ChangeImageColor fromImage:colorChangeImageView.image currentColor:currentColor newColor:someCell.contentView.backgroundColor];
        currentColor = someCell.contentView.backgroundColor;
        colorChangeImageView.image = newImage;
    }
    
}

#pragma mark AKPickerView Delegates

- (NSUInteger)numberOfItemsInPickerView:(AKPickerView *)pickerView
{
    return [self.filterTitles count];
}

- (NSString *)pickerView:(AKPickerView *)pickerView titleForItem:(NSInteger)item
{
    return self.filterTitles[item];
}

- (void)pickerView:(AKPickerView *)pickerView didSelectItem:(NSInteger)item
{
    NSLog(@"%@", self.filterTitles[item]);
}


#pragma mar ColorPicker Delegates

-(void)didSelectedColor:(UIColor *)color{
    selectedColor = color;
    [_collectionView reloadData];
}

#pragma mark Actions
- (void) goBack{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void) finishEditing{
    [self.navigationController pushViewController:yourDataViewController animated:YES];
    
}
- (void)segmentedControlValueChanged:(UISegmentedControl*)sender
{
    if (sender.selectedSegmentIndex == 0) {
        editorViewSlideIsUp = NO;
        [self formSegmentIsSelected];
    }else if (sender.selectedSegmentIndex == 1) {
        editorViewSlideIsUp = NO;
        isToShowMaterials = YES;
        [self materialsSegmentIsSelected];
        
    }else if (sender.selectedSegmentIndex == 2) {
        editorViewSlideIsUp = YES;
        isToShowMaterials = NO;
    }
    [_collectionView reloadData];
    [self showAccessories];
}
- (void) drawShoePart2{
    
    UIBezierPath* bezierPath = UIBezierPath.bezierPath;
    [bezierPath moveToPoint: CGPointMake(126.5, 119.5)];
    [bezierPath addCurveToPoint: CGPointMake(189.5, 119.5) controlPoint1: CGPointMake(142.25, 119.5) controlPoint2: CGPointMake(189.5, 119.5)];
    [bezierPath addCurveToPoint: CGPointMake(244.5, 158.5) controlPoint1: CGPointMake(189.5, 119.5) controlPoint2: CGPointMake(277.5, 143.5)];
    [bezierPath addCurveToPoint: CGPointMake(126.5, 119.5) controlPoint1: CGPointMake(211.5, 173.5) controlPoint2: CGPointMake(244.5, 158.5)];
    
    shapeLayer1 = [CAShapeLayer layer];
    shapeLayer1.fillRule = kCAFillRuleEvenOdd;
    shapeLayer1.fillColor = [UIColor redColor].CGColor;
    shapeLayer1.path = [bezierPath CGPath];
    
    //    [shoeEditorView.layer addSublayer:shapeLayer1];
}

- (void) drawShoePart1{
    
    
    UIBezierPath* bezierPath = UIBezierPath.bezierPath;
    [bezierPath moveToPoint: CGPointMake(13.5, 40.5)];
    [bezierPath addCurveToPoint: CGPointMake(25.5, 80.5) controlPoint1: CGPointMake(13.5, 40.5) controlPoint2: CGPointMake(14.5, 80.5)];
    [bezierPath addCurveToPoint: CGPointMake(130.5, 80.5) controlPoint1: CGPointMake(36.5, 80.5) controlPoint2: CGPointMake(53.5, 98.5)];
    [bezierPath addCurveToPoint: CGPointMake(198.5, 66.5) controlPoint1: CGPointMake(207.5, 62.5) controlPoint2: CGPointMake(178.5, 73.5)];
    [bezierPath addCurveToPoint: CGPointMake(217.5, 40.5) controlPoint1: CGPointMake(218.5, 59.5) controlPoint2: CGPointMake(217.5, 40.5)];
    [bezierPath addCurveToPoint: CGPointMake(13.5, 40.5) controlPoint1: CGPointMake(217.5, 40.5) controlPoint2: CGPointMake(1.5, 58.5)];
    [bezierPath addCurveToPoint: CGPointMake(13.5, 40.5) controlPoint1: CGPointMake(25.5, 22.5) controlPoint2: CGPointMake(13.5, 40.5)];
    [bezierPath closePath];
    
    shapeLayer = [CAShapeLayer layer];
    shapeLayer.fillRule = kCAFillRuleEvenOdd;
    shapeLayer.fillColor = [UIColor redColor].CGColor;
    shapeLayer.path = [bezierPath CGPath];
    
    //    [shoeEditorView.layer addSublayer:shapeLayer];
    
}

- (void) colorRefillWithColor:(UIColor*)theColor{
    
    shapeLayer.fillColor = theColor.CGColor;
    shapeLayer1.fillColor = theColor.CGColor;
    
}

- (void) showAccessories{
    
    [UIView animateWithDuration:0.6f
                          delay:0.1f
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         if (segControl.selectedSegmentIndex == 2) {
                             shoeEditorView.hidden = YES;
                         }
                         [self startAnimationWithDirection:editorViewSlideIsUp];
                     }
                     completion:^(BOOL finished){
                         if (!editorViewSlideIsUp) {
                             isToShowMaterials = FALSE;
                             shoeEditorView.hidden = NO;
                         }
                     }
     ];
    
}

- (void) startAnimationWithDirection:(BOOL)up {
    if (up) {
        [editorFilterView setFrame:CGRectMake(0, segControl.frame.origin.y + segControl.frame.size.height, editorFilterView.frame.size.width, editorFilterView.frame.size.height)];
        [beltCollectionView setFrame:CGRectMake(0, beltCollectionView.frame.origin.y, beltCollectionView.frame.size.width, beltCollectionView.frame.size.height)];
    }else{
        [editorFilterView setFrame:CGRectMake(0, screenSize.height, screenSize.width, 100)];
        [beltCollectionView setFrame:CGRectMake(screenSize.width, beltCollectionView.frame.origin.y, beltCollectionView.frame.size.width, beltCollectionView.frame.size.height)];
    }
    //    editorViewSlideIsUp = !up;
}

- (void) showPlatformAdjuster{
    ShoeFormDetailViewController *showFormDetailVC = [[ShoeFormDetailViewController alloc] init];
    [self.navigationController pushViewController:showFormDetailVC animated:YES];
    
}
- (void) formSegmentIsSelected {

    setShoeInsideColor.hidden = YES;
    setShoeInsideColor.enabled = NO;
    setShoeLeftAndNoseColor.hidden = YES;
    setShoeLeftAndNoseColor.enabled = NO;
    setPlatformColor.hidden = YES;
    setPlatformColor.enabled = NO;
    setRightColor.hidden = YES;
    setRightColor.enabled = NO;
    
    changeNoseMaterial.hidden = NO;
    changeNoseMaterial.enabled = YES;
    changePlatformHeight.hidden = NO;
    changePlatformHeight.enabled = YES;
}
- (void) materialsSegmentIsSelected {
    
    setShoeInsideColor.hidden = NO;
    setShoeInsideColor.enabled = YES;
    setShoeLeftAndNoseColor.hidden = NO;
    setShoeLeftAndNoseColor.enabled = YES;
    setPlatformColor.hidden = NO;
    setPlatformColor.enabled = YES;
    setRightColor.hidden = NO;
    setRightColor.enabled = YES;

    changeNoseMaterial.hidden = YES;
    changeNoseMaterial.enabled = NO;
    changePlatformHeight.hidden = YES;
    changePlatformHeight.enabled = NO;

}
- (void) showMaterials{
    isToShowMaterials = TRUE;
    [_collectionView reloadData];
     editorViewSlideIsUp = YES;
    [self showAccessories];
}
- (void)setColorOfPartWithSender:(id)sender{
    
    switch ([sender tag]) {
        case 1:
            colorChangeImageView = shoeInsideImageView;
            break;
        case 2:
            colorChangeImageView = shoeLeftAndNose;
            break;
        case 3:
            colorChangeImageView = shoePlatformView;
            break;
        case 4:
            colorChangeImageView = shoeRightImageView;
            break;
        default:
            break;
    }
    [self showMaterials];
    
}
#pragma mark TOUCH HANDLING
- (void) setupTouches {

    UISwipeGestureRecognizer *upRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandle:)];
    upRecognizer.direction = UISwipeGestureRecognizerDirectionUp;
    [upRecognizer setNumberOfTouchesRequired:1];
    
    //add the your gestureRecognizer , where to detect the touch..
    [self.view addGestureRecognizer:upRecognizer];

    
    UISwipeGestureRecognizer *downRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandle:)];
    downRecognizer.direction = UISwipeGestureRecognizerDirectionDown;
    [downRecognizer setNumberOfTouchesRequired:1];
    
    [self.view addGestureRecognizer:downRecognizer];
}

- (void)swipeHandle:(UISwipeGestureRecognizer*)gestureRecognizer{
   
    if (!isInResultView) {
        return;
    }

    if (gestureRecognizer.direction == UISwipeGestureRecognizerDirectionUp) {
                NSLog(@"UP");
        [UIView animateWithDuration:0.3
                              delay:0
                            options: UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             CGRect rect = resultFloatingView.frame;
                             if (rect.origin.y > screenSize.height - rect.size.height) {
                                 rect.origin.y -= 114;
                                 resultFloatingView.frame = rect;
                             }
                         }
                         completion:^(BOOL finished){}];
    
 
    }else if (gestureRecognizer.direction == UISwipeGestureRecognizerDirectionDown){
        NSLog(@"DOWN");
        [UIView animateWithDuration:0.3
                              delay:0
                            options: UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             CGRect rect = resultFloatingView.frame;
                             if (rect.origin.y <= screenSize.height - rect.size.height) {
                                 rect.origin.y += 114;
                                 resultFloatingView.frame = rect;
                             }
                         }
                         completion:^(BOOL finished){
                             
                         }];
    }
    
    
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint location = [touch locationInView:self.view];
    CGRect frame = [draggerView convertRect:draggerView.bounds toView:self.view];
    if (CGRectContainsPoint(frame, location)) {
        NSLog(@"Contains");
        isInResultView = TRUE;
    }

}
-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    isInResultView = FALSE;
    
}
-(void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event{
    isInResultView = FALSE;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
