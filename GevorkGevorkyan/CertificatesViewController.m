//
//  CertificatesViewController.m
//  GevorkGevorkyan
//
//  Created by Garnik Ghazaryan on 12/3/14.
//  Copyright (c) 2014 Garnik Ghazaryan. All rights reserved.
//

#import "CertificatesViewController.h"
#import "Utils.h"
#import "MyOrdersViewController.h"
@interface CertificatesViewController ()

@end

@implementation CertificatesViewController{
    
    CGSize screenSize;
    UIImageView *certificateImageView;
    UITextField *nameTextField;
    UIButton *submitButton;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    self.view.backgroundColor = rgba(250, 249, 245, 1);
    self.title = @"Руслан Сапожников";
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithTitle:@"МЕНЮ" style:UIBarButtonItemStyleBordered target:self action:@selector(goBack)];
    [leftItem  setTitleTextAttributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:14] } forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    
    screenSize = [Utils getScreenBounds];
    [self uiConfigurations];
}

- (void) uiConfigurations{
    certificateImageView = [Utils createImageViewWithRect:self.view.bounds image:[UIImage imageNamed:@"certificateView.png"]];
    nameTextField = [Utils createTextFieldWithRect:CGRectMake(30, certificateImageView.bounds.origin.y + 298, 255, 41) title:@"" placeHolderText:@"Виктория Каблукова"];
    submitButton = [Utils createButtonInRect:CGRectMake(30, certificateImageView.bounds.size.height - 81, 253, 42) title:@"" target:self action:@selector(submitCertificate) backGroundImage:[UIImage imageNamed:@"submitCertificateImage.png"] highlightImage:nil];
    
    nameTextField.background    = [UIImage imageNamed:@"textFieldBg.png"];
    nameTextField.textAlignment = NSTextAlignmentCenter;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark Actions

- (void) goBack{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void) submitCertificate{
    MyOrdersViewController *myOrdersVC = [[MyOrdersViewController alloc] init];
    [self.navigationController pushViewController:myOrdersVC animated:YES];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
