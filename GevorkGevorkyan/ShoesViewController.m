//
//  ShoesViewController.m
//  GevorkGevorkyan
//
//  Created by Garnik Ghazaryan on 10/26/14.
//  Copyright (c) 2014 Garnik Ghazaryan. All rights reserved.
//

#import "ShoesViewController.h"
#import "Utils.h"
#import "CatalogViewCell.h"
@interface ShoesViewController ()

@end

@implementation ShoesViewController{

    UIScrollView *bgScrollView;
    UIScrollView *shoeImageScrollView;
    UIPageControl* pageControl;
    UIImageView *shoeImage;
    UILabel *itemNameLabel;
    UILabel *itemPriceLabel;
    UILabel *similarModelsLabel;
    UILabel *dotetlabel;
    UILabel *scratchedPriceLabel;
    UITextView *itemDescrTextView;
    UIButton *makeOrderButton;
    UIButton *saveScratchButton;
    UICollectionView *_collectionView;
    NSArray *shoeImageNameArray;
    
    
    CGSize screenSize;
    BOOL pageControlBeingUsed;
    int pageCount;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    self.view.backgroundColor = rgba(250, 249, 245, 1);
    self.title = @"ТУФЛИ";
    self.navigationController.navigationBar.backgroundColor = [UIColor whiteColor];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithTitle:@"НАЗАД" style:UIBarButtonItemStyleBordered target:self action:@selector(goBack)];
    [leftItem  setTitleTextAttributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:14] } forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithTitle:@"РЕДАКТОР" style:UIBarButtonItemStyleBordered target:self action:@selector(showEditor)];
    [rightItem  setTitleTextAttributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:14.0] } forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = rightItem;
    self.navigationController.navigationBar.tintColor = [UIColor grayColor];
    
    screenSize = [Utils getScreenBounds];    // Do any additional setup after loading the view.
    [self uiConfigurations];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) uiConfigurations{

    pageCount = 5;

    bgScrollView = [[UIScrollView alloc]initWithFrame:self.view.bounds];
    bgScrollView.contentSize = CGSizeMake(bgScrollView.frame.size.width, bgScrollView.frame.size.height + 100);
    [self.view addSubview:bgScrollView];
    
    shoeImageScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, screenSize.width, 240)];
    shoeImageScrollView.contentSize = CGSizeMake(shoeImageScrollView.frame.size.width * pageCount, shoeImageScrollView.frame.size.height);
    shoeImageScrollView.delegate = self;
    shoeImageScrollView.backgroundColor = [UIColor whiteColor];
    shoeImageScrollView.pagingEnabled = YES;
    shoeImageScrollView.showsHorizontalScrollIndicator = NO;
    [bgScrollView addSubview:shoeImageScrollView];
    shoeImageNameArray = [NSArray arrayWithObjects:@"reddishShoes.png",@"shoes.png",@"bgImage.png",@"loginBG.png",@"placeHolderImage.png", nil];
    
    for (int i = 0; i < shoeImageNameArray.count ; i++)
    {
        UIImage *images = [UIImage imageNamed:[shoeImageNameArray objectAtIndex:i]];
        shoeImage = [[UIImageView alloc] initWithImage:images];
        shoeImage.clipsToBounds = YES;
        shoeImage.frame = CGRectMake( shoeImageScrollView.frame.size.width * i, 0, shoeImageScrollView.frame.size.width, shoeImageScrollView.frame.size.height);
        [shoeImageScrollView addSubview:shoeImage];
    }
    pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, shoeImageScrollView.frame.size.height - 40, screenSize.width, 30)];
    pageControl.currentPage = 0;
    pageControl.numberOfPages = pageCount;
    [pageControl addTarget:self action:@selector(changePage) forControlEvents:UIControlEventValueChanged];
    pageControl.backgroundColor = [UIColor clearColor];
    pageControl.currentPageIndicatorTintColor = rgba(231, 198, 128, 1);
    pageControl.pageIndicatorTintColor = rgba(251, 241, 224, 1);
    [bgScrollView addSubview:pageControl];
    
    [Utils setViewToAddOn:bgScrollView];
    itemNameLabel = [Utils createLabelWithRect:CGRectMake(10, shoeImageScrollView.frame.size.height + 10, shoeImageScrollView.frame.size.width/2 + 40, 60) title:@"PUMPS IN REDDISH PURPLE METAL PATENT LEATHER"];
    itemNameLabel.numberOfLines = 3;
    itemNameLabel.font = [UIFont systemFontOfSize:16];

    dotetlabel = [Utils createLabelWithRect:CGRectMake( itemNameLabel.frame.size.width, itemNameLabel.frame.origin.y + 20, itemNameLabel.frame.size.height,30) title:@"............."];
    dotetlabel.transform = CGAffineTransformMakeRotation(M_PI_2);
    dotetlabel.lineBreakMode = NSLineBreakByWordWrapping;
    dotetlabel.numberOfLines = 100;

    itemPriceLabel = [Utils createLabelWithRect:CGRectMake(dotetlabel.frame.origin.x, itemNameLabel.frame.origin.y, 100, 40) title:@"16 950 P"];
    itemPriceLabel.textAlignment = NSTextAlignmentCenter;

    scratchedPriceLabel = [Utils createLabelWithRect:CGRectMake(dotetlabel.frame.origin.x, itemNameLabel.frame.origin.y + itemPriceLabel.frame.size.height - 10, 100, 40) title:@"16 950 P"];
    scratchedPriceLabel.textAlignment = NSTextAlignmentCenter;

    itemDescrTextView = [Utils createTextViewWithRect:CGRectMake(10,itemNameLabel.frame.origin.y + itemNameLabel.frame.size.height + 10 , shoeImageScrollView.frame.size.width - 10, 60) contentText:@"Reddish purple metal patent leather 12 cm stiletto heel in patent leather 3 cm maxiplatform in patent leather leather sole with rubber island." ];
    itemDescrTextView.backgroundColor = rgba(250, 249, 245, 1);
    
    makeOrderButton = [Utils createButtonInRect:CGRectMake(10, itemDescrTextView.frame.origin.y + 100, screenSize.width - 10, 40) title:@"" target:self action:@selector(makeOrder) backGroundImage:[UIImage imageNamed:@"makeOrderImage.png"] highlightImage:nil];
    saveScratchButton = [Utils createButtonInRect:CGRectMake(10, makeOrderButton.frame.origin.y + 80, screenSize.width - 10, 40) title:@"" target:self action:@selector(saveInScratch) backGroundImage:[UIImage imageNamed:@"saveScratchImage.png"] highlightImage:nil];
    similarModelsLabel = [Utils createLabelWithRect:CGRectMake(10, saveScratchButton.frame.origin.y + 45, screenSize.width - 10, 30) title:@"ПОХОЖИЕ МОДЕЛИ"];
    
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    
    _collectionView=[[UICollectionView alloc] initWithFrame:CGRectMake(5, similarModelsLabel.frame.origin.y + 5, screenSize.width - 10, 140) collectionViewLayout:layout];
    [_collectionView setDataSource:self];
    [_collectionView setDelegate:self];
//    [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    _collectionView.backgroundColor = bgScrollView.backgroundColor;
    [layout setItemSize:CGSizeMake(100, 110)];
    [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    _collectionView.showsHorizontalScrollIndicator = NO;
    [bgScrollView addSubview:_collectionView];
}
-(void)viewWillAppear:(BOOL)animated{
    
    [_collectionView registerNib:[UINib nibWithNibName:@"CatalogViewCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"myCellIdentifier"];
}
-(void)viewDidAppear:(BOOL)animated{

}
#pragma mark Collection view delegates;
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 15;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CatalogViewCell *cell = (CatalogViewCell*) [collectionView dequeueReusableCellWithReuseIdentifier:@"myCellIdentifier" forIndexPath:indexPath];
    cell.layer.borderWidth = 1.5f;
    cell.layer.cornerRadius = 5.f;
    cell.layer.borderColor = rgba(170, 178, 189, 1).CGColor;
    cell.catalogCellPriceLabel.hidden = YES;
    [cell.catalogCellImageView removeFromSuperview];
    UIImageView *imageview = [[UIImageView alloc] initWithFrame:cell.bounds];
    imageview.image = [UIImage imageNamed:@"shoes.png"];
    [cell addSubview:imageview];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(80, 80);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 2.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 7.0;
}

// Layout: Set Edges
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    // return UIEdgeInsetsMake(0,8,0,8);  // top, left, bottom, right
    return UIEdgeInsetsMake(0,5,0,5);  // top, left, bottom, right
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSLog(@"Selected items # %ld",(long)indexPath.row);
}

#pragma mark Scrollview delegate
- (void)scrollViewDidScroll:(UIScrollView *)sender {
    if (!pageControlBeingUsed) {
        // Switch the indicator when more than 50% of the previous/next page is visible
        CGFloat pageWidth = shoeImageScrollView.frame.size.width;
        int page = floor((shoeImageScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        pageControl.currentPage = page;
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    pageControlBeingUsed = NO;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    pageControlBeingUsed = NO;
}

- (void)changePage {
    // Update the scroll view to the appropriate page
    CGRect frame;
    frame.origin.x = shoeImageScrollView.frame.size.width * pageControl.currentPage;
    frame.origin.y = 0;
    frame.size = shoeImageScrollView.frame.size;
    [shoeImageScrollView scrollRectToVisible:frame animated:YES];
    
    // Keep track of when scrolls happen in response to the page control
    // value changing. If we don't do this, a noticeable "flashing" occurs
    // as the the scroll delegate will temporarily switch back the page
    // number.
    pageControlBeingUsed = YES;
}
#pragma mark Actions
- (void) goBack{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) showEditor{

    
}
- (void) makeOrder{
    
}
- (void) saveInScratch{
    
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
