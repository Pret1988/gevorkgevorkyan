//
//  PlatformHeightAdjustView.h
//  GevorkGevorkyan
//
//  Created by Garnik Ghazaryan on 11/21/14.
//  Copyright (c) 2014 Garnik Ghazaryan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlatformHeightAdjustView : UIView

@property (nonatomic) BOOL isSwitchOn;
@end
