//
//  ReadyShoeViewController.m
//  GevorkGevorkyan
//
//  Created by Garnik Ghazaryan on 12/2/14.
//  Copyright (c) 2014 Garnik Ghazaryan. All rights reserved.
//

#import "ReadyShoeViewController.h"
#import "Utils.h"
@interface ReadyShoeViewController ()

@end

@implementation ReadyShoeViewController{
    
    UIImageView *shoeImageView;
    UILabel *titleLabel;
    UITextView *descriptionView;
    UIButton *detailedInfoButton;
    
    CGSize screenSize;

}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    self.view.backgroundColor = rgba(245, 243, 236, 1);
    self.title = @"ЗАКАЗ № 54023\nОБУВЬ ГОТОВА";
    self.navigationController.navigationBar.backgroundColor = [UIColor whiteColor];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithTitle:@"НАЗАД" style:UIBarButtonItemStyleBordered target:self action:@selector(goBack)];
    [leftItem  setTitleTextAttributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:14]} forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem = leftItem;
    // Do any additional setup after loading the view.
    self.navigationController.navigationBar.tintColor = [UIColor grayColor];
    
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithTitle:@"РЕДАКТОР" style:UIBarButtonItemStyleBordered target:self action:@selector(showEditor)];
    [rightItem  setTitleTextAttributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:14.0] } forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = rightItem;
    self.navigationController.navigationBar.tintColor = [UIColor grayColor];

    screenSize = [Utils getScreenBounds];
    [self uiConfigurations];

    // Do any additional setup after loading the view.
}
- (void) uiConfigurations{
    
    [Utils setViewToAddOn:self.view];
    shoeImageView = [Utils createImageViewWithRect:CGRectMake(0, 0, screenSize.width, 290) image:[UIImage imageNamed:@"readyShoeImage.png"]];
    titleLabel = [Utils createLabelWithRect:CGRectMake(17, shoeImageView.frame.origin.y + shoeImageView.frame.size.height - 5, 200, 30) title:@"Описание этапа"];
    titleLabel.font = [UIFont fontWithName:@"GothamPro-Medium" size:17];
    titleLabel.textColor = rgba(78, 60, 54, 1);
    descriptionView = [Utils createTextViewWithRect:CGRectMake(15, titleLabel.frame.origin.y + titleLabel.frame.size.height, screenSize.width, 100) contentText:@"Но чтобы вы поняли, откуда возникает это превратное представление людей, порицающих наслаждение и восхваляющих страдания, я раскрою перед вами всю картину и разъясню, что именно говорил этот человек, открывший истину, которого я бы назвал зодчим счастливой жизни. Действительно, никто не отвергает."];
    descriptionView.font = [UIFont fontWithName:@"GothamPro" size:13];
    descriptionView.textColor = rgba(78, 60, 54, 1);
    descriptionView.backgroundColor = [UIColor clearColor];
    detailedInfoButton = [Utils createButtonInRect:CGRectMake(15, descriptionView.frame.origin.y + descriptionView.frame.size.height+10, screenSize.width - 30, 40) title:@"" target:self action:@selector(detailedInfoPressed) backGroundImage:[UIImage imageNamed:@"detailedInfoButtonImage.png"] highlightImage:nil];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark Actions

- (void) goBack{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void) showEditor{
    
    
}
- (void) detailedInfoPressed{
    
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
