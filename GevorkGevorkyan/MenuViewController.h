//
//  MenuViewController.h
//  GevorkGevorkyan
//
//  Created by Garnik Ghazaryan on 10/17/14.
//  Copyright (c) 2014 Garnik Ghazaryan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainMenuView.h"
#import "iCarousel.h"

@interface MenuViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, MainMenuViewDelegate, iCarouselDataSource, iCarouselDelegate>

@end
