//
//  FootSizeAdjustViewController.h
//  GevorkGevorkyan
//
//  Created by Garnik Ghazaryan on 12/5/14.
//  Copyright (c) 2014 Garnik Ghazaryan. All rights reserved.
//


#import <UIKit/UIKit.h>

@protocol FootSizeAdjustDelegate <NSObject>

- (void)didFinishSizeEditingWithText:(NSString*) inputSize;

@end

@interface FootSizeAdjustViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIImageView *bodyImageView;
@property (strong, nonatomic) IBOutlet UITextField *sizeTextField;
@property (strong, nonatomic) IBOutlet UILabel *descriptionLabel;

@property (nonatomic) id delegate;
@property (nonatomic) NSInteger selectedItemAtIndex;
@end
