//
//  MySketchesViewController.m
//  GevorkGevorkyan
//
//  Created by Garnik Ghazaryan on 12/2/14.
//  Copyright (c) 2014 Garnik Ghazaryan. All rights reserved.
//

#import "MySketchesViewController.h"
#import "Utils.h"
#import "MyOrdersTableViewCell.h"

@interface MySketchesViewController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation MySketchesViewController {

    CGSize screenSize;
    UITableView *sketchTableView;
    NSMutableArray *currentSketches;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    self.view.backgroundColor = rgba(245, 243, 236, 1);
    self.title = @"МОИ ЭСКИЗЫ";
    self.navigationController.navigationBar.backgroundColor = [UIColor whiteColor];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithTitle:@"МЕНЮ" style:UIBarButtonItemStyleBordered target:self action:@selector(goBack)];
    [leftItem  setTitleTextAttributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:14]} forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem = leftItem;
    // Do any additional setup after loading the view.
    self.navigationController.navigationBar.tintColor = [UIColor grayColor];
    screenSize = [Utils getScreenBounds];
    [self uiConfigurations];
}
- (void) uiConfigurations{
    
    sketchTableView = [[UITableView alloc] initWithFrame:self.view.frame style:UITableViewStyleGrouped];
    sketchTableView.delegate = self;
    sketchTableView.dataSource = self;
    sketchTableView.backgroundColor = [UIColor clearColor];
    //    orderTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:sketchTableView];
    
    currentSketches = [NSMutableArray arrayWithArray:@[@"Женские ТУФЛИ по заказу #1",@"Женские ТУФЛИ по заказу #2"]];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark Tableview delegate and datasourse

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section

{
    if (section == 0) {
        return [currentSketches count];
    }else return 0;
    
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 120;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 120;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    static NSString *cellIdentifier = @"myOrdersTableViewCell";
    
    
    MyOrdersTableViewCell *cell = (MyOrdersTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MyOrdersTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    if (indexPath.section == 0) {
        cell.orderNumberLabel.text = @"24.02.2014";
        cell.orderItemNameLabel.text = [currentSketches objectAtIndex:indexPath.row];
        cell.orderStatusLabel.text = @"Текущая сумма:";
        cell.orderPriceLabel.text = @"8 410 Р";
        cell.orderCellShoeImage.image = [UIImage imageNamed:@"customShoe.png"];
    }
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{
    if (indexPath.section == 0) {
//        [self.navigationController pushViewController:checkOutVC animated:YES];
    }
}

#pragma mark Actions

- (void) goBack{
    [self.navigationController popViewControllerAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
