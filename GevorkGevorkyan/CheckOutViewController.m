//
//  CheckOutViewController.m
//  GevorkGevorkyan
//
//  Created by Garnik Ghazaryan on 11/30/14.
//  Copyright (c) 2014 Garnik Ghazaryan. All rights reserved.
//

#import "CheckOutViewController.h"
#import "CheckOutTableViewCell.h"
#import "Utils/Utils.h"
@interface CheckOutViewController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation CheckOutViewController  {
    
    CGSize screenSize;
    
    UITableView *sheetTableView;
    UILabel *pageTitleLabel;
    UIImageView *totalBgImage;
    UILabel *totalSumLabel;
    UILabel *totalSumbNumber;
    UIButton *submitButton;
    
    UIImageView *statusOfPreparationImageView;
    
    NSMutableArray *formElements;
    NSMutableArray *materialElements;
    NSMutableArray *pricesArray;
    
    
    NSMutableAttributedString *section1Line0;
    NSMutableAttributedString *section1Line1;
    NSMutableAttributedString *section1Line2;
    NSMutableAttributedString *section1Line3;
    
    NSMutableAttributedString *section2Line0;
    NSMutableAttributedString *section2Line1;
    NSMutableAttributedString *section2Line2;
    NSMutableAttributedString *section2Line3;
    NSMutableAttributedString *section2Line4;
    NSMutableAttributedString *section2Line5;
    NSMutableAttributedString *section2Line6;
    
    NSString *noseParam;
    NSString *climbParam;
    NSString *heelHeight;
    NSString *platformHeight;
    
    NSString *outsideMaterial;
    NSString *insideMaterial;
    NSString *layerMaterial;
    NSString *heelMaterial;
    NSString *platformMarterial;
    NSString *soleMaterial;
    
    UILabel *tmpLabel;
    UIImageView *bgImageView;
}
- (id)init
{
    self = [super init];
    if (self) {
        screenSize = [Utils getScreenBounds];
        [self uiConfigurations];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    self.view.backgroundColor = rgba(252, 252, 252, 1);
    self.title = @"ВАШ ЗАКАЗ";
    self.navigationController.navigationBar.backgroundColor = [UIColor whiteColor];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithTitle:@"НАЗАД" style:UIBarButtonItemStyleBordered target:self action:@selector(goBack)];
    [leftItem  setTitleTextAttributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:14] } forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem = leftItem;
    // Do any additional setup after loading the view.

}

- (void) uiConfigurations{
    
    [Utils setViewToAddOn:self.view];
    pageTitleLabel = [Utils createLabelWithRect:CGRectMake(screenSize.width/2 - 150, 54, 300, 40) title:@"ПАРАМЕТРЫ ОБУВИ"];
    pageTitleLabel.textAlignment = NSTextAlignmentCenter;
    pageTitleLabel.textColor = rgba(231, 198, 128, 1);
    pageTitleLabel.font = [UIFont fontWithName:@"GothamPro-Bold" size:19.0];
    
    
    sheetTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, pageTitleLabel.frame.origin.y + pageTitleLabel.frame.size.height -10, screenSize.width, screenSize.height - 195) style:UITableViewStyleGrouped];
    sheetTableView.delegate = self;
    sheetTableView.dataSource = self;
    sheetTableView.backgroundColor = [UIColor clearColor];
    sheetTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:sheetTableView];
    
    totalBgImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, sheetTableView.frame.origin.y + sheetTableView.frame.size.height, screenSize.width, 62)];
    totalBgImage.image = [UIImage imageNamed:@"checkOutTotalBgImage.png"];
    [self.view addSubview:totalBgImage];
    
    totalSumLabel = [[UILabel alloc] initWithFrame:CGRectMake(8, totalBgImage.frame.size.height/2 - 15, 150, 30)];
    totalSumLabel.text = @"ИТОГОВАЯ СУММА:";
    totalSumLabel.font = [UIFont fontWithName:@"GothamPro" size:15];
    
    totalSumbNumber = [[UILabel alloc] initWithFrame:CGRectMake(screenSize.width - 100, totalBgImage.frame.size.height/2 - 20, 92, 40)];
    totalSumbNumber.font = [UIFont fontWithName:@"GothamPro" size:20];
    totalSumbNumber.textColor = rgba(55, 188, 155, 1);
    totalSumbNumber.text = @"13 100 Р";
    
    [totalBgImage addSubview:totalSumLabel];
    [totalBgImage addSubview:totalSumbNumber];
    
    submitButton = [Utils createButtonInRect:CGRectMake(0, totalBgImage.frame.origin.y + totalBgImage.frame.size.height, screenSize.width, 51) title:@"" target:self action:@selector(submitOrder) backGroundImage:[UIImage imageNamed:@"submitOrderButton.png"] highlightImage:nil];
    [self.view addSubview:submitButton];
    
    statusOfPreparationImageView = [Utils createImageViewWithRect:CGRectMake(0, screenSize.height - 49, screenSize.width, 50) image:[UIImage imageNamed:@"prepairingStatus.png"]];
    statusOfPreparationImageView.hidden = YES;
    
    noseParam = @"Закрытый";
    climbParam = @"Полуоткрытый ";
    heelHeight = @"6 см";
    platformHeight = @"2 см";
    
    outsideMaterial = @"Кожа (экз., красный)";
    insideMaterial = @"Замша";
    layerMaterial = @"Замша (черный)";
    heelMaterial = @"Дерево (бежевый)";
    platformMarterial = @"Дерево (бежевый)";
    soleMaterial = @"Резина (красный)";
    
    section1Line0 = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"Нос: %@",noseParam]];
    [section1Line0 addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:17] range:NSMakeRange(0, 4)];
    [section1Line0 addAttribute:NSForegroundColorAttributeName value:rgba(78, 60, 54, 1) range:NSMakeRange(0, 4)];
    
    section1Line1 = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"Подъем: %@",climbParam]];
    [section1Line1 addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:17] range:NSMakeRange(0, 6)];
    [section1Line1 addAttribute:NSForegroundColorAttributeName value:rgba(78, 60, 54, 1) range:NSMakeRange(0, 6)];
    
    section1Line2 = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"Высота каблука: %@",heelHeight]];
    [section1Line2 addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:17] range:NSMakeRange(0, 14)];
    [section1Line2 addAttribute:NSForegroundColorAttributeName value:rgba(78, 60, 54, 1) range:NSMakeRange(0, 14)];
    
    section1Line3 = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"Высота платформы: %@",platformHeight]];
    [section1Line3 addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:17] range:NSMakeRange(0, 16)];
    [section1Line3 addAttribute:NSForegroundColorAttributeName value:rgba(78, 60, 54, 1) range:NSMakeRange(0, 16)];
    
    
    section2Line0 = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"Внешний: %@",outsideMaterial]];
    [section2Line0 addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:17] range:NSMakeRange(0, 7)];
    [section2Line0 addAttribute:NSForegroundColorAttributeName value:rgba(78, 60, 54, 1) range:NSMakeRange(0, 7)];
    
    section2Line1 = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"+ Кожа (бордовый):"]];
    [section2Line1 addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:17] range:NSMakeRange(0, section2Line1.length)];
    [section2Line1 addAttribute:NSForegroundColorAttributeName value:rgba(78, 60, 54, 1) range:NSMakeRange(0, section2Line1.length)];
    
    section2Line2 = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"Внутренний: %@",insideMaterial]];
    [section2Line2 addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:17] range:NSMakeRange(0, 10)];
    [section2Line2 addAttribute:NSForegroundColorAttributeName value:rgba(78, 60, 54, 1) range:NSMakeRange(0, 10)];
    
    section2Line3 = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"Стелька: %@",layerMaterial]];
    [section2Line3 addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:17] range:NSMakeRange(0, 7)];
    [section2Line3 addAttribute:NSForegroundColorAttributeName value:rgba(78, 60, 54, 1) range:NSMakeRange(0, 7)];
    
    section2Line4 = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"Каблук: %@",heelMaterial]];
    [section2Line4 addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:17] range:NSMakeRange(0, 6)];
    [section2Line4 addAttribute:NSForegroundColorAttributeName value:rgba(78, 60, 54, 1) range:NSMakeRange(0, 6)];
    
    section2Line5 = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"Платформа: %@",platformMarterial]];
    [section2Line5 addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:17] range:NSMakeRange(0, 9)];
    [section2Line5 addAttribute:NSForegroundColorAttributeName value:rgba(78, 60, 54, 1) range:NSMakeRange(0, 9)];
    
    section2Line6 = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"Подошва: %@",soleMaterial]];
    [section2Line6 addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:17] range:NSMakeRange(0, 7)];
    [section2Line6 addAttribute:NSForegroundColorAttributeName value:rgba(78, 60, 54, 1) range:NSMakeRange(0, 7)];

    
    formElements = [NSMutableArray arrayWithArray:@[section1Line0,section1Line1,section1Line2,section1Line3]];
    materialElements = [NSMutableArray arrayWithArray:@[section2Line0,section2Line1,section2Line2,section2Line3,section2Line4,section2Line5,section2Line6]];
    pricesArray = [[NSMutableArray alloc] initWithCapacity:2];
    [pricesArray addObject:@[@"2000 Р",@"600 Р",@"600 Р",@"200 Р"]];
    [pricesArray addObject:@[@"5000 Р",@"1000 Р",@"1500 Р",@"800 Р",@"800 Р",@"800 Р",@"800 Р"]];
    
}

#pragma mark Tableview delegate and datasourse

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 2;

}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section

{
    if (section == 0) {
        return [formElements count];
    }else if (section == 1){
        return [materialElements count];
    }else return 0;
    
}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    if (section == 0) {
        return @"ФОРМА1";
    }else if (section == 1){
        return @"МАТЕРИАЛ1";
    }else return 0;

}
- (CGFloat)tableView:(UITableView *)tableView
           estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 24;
}

- (CGFloat)tableView:(UITableView *)tableView 
           heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 24;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *sectionView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)];
    [sectionView setBackgroundColor:[UIColor clearColor]];
    
    UILabel *tempLabel=[[UILabel alloc]initWithFrame:CGRectMake(8,15, 300, 30)];
    tempLabel.backgroundColor=[UIColor clearColor];
    tempLabel.textColor = rgba(231, 198, 128, 1); //here you can change the text color of header.

    if (section == 0) {
        tempLabel.text = @"ФОРМА";
        tempLabel.frame = CGRectMake(15, 22, 300, 30);
    }else if (section == 1){
        tempLabel.text = @"МАТЕРИАЛ";
        tempLabel.frame = CGRectMake(15, 5, 300, 30);
    }
    
    [sectionView addSubview:tempLabel];

    
    return sectionView;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    static NSString *cellIdentifier = @"checkOutCellIdentifier";
    
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
//    if (cell == nil) {
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
//    }
    CheckOutTableViewCell *cell = (CheckOutTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CheckOutTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    if (indexPath.section == 0) {
        cell.leftLabel.attributedText = [formElements objectAtIndex:indexPath.row];
        cell.rightLabel.text = [[pricesArray objectAtIndex:0] objectAtIndex:indexPath.row];
    }else if (indexPath.section == 1){
        cell.leftLabel.attributedText = [materialElements objectAtIndex:indexPath.row];
        cell.rightLabel.text = [[pricesArray objectAtIndex:1] objectAtIndex:indexPath.row];
    }
    [cell.leftLabel setContentMode:UIViewContentModeScaleAspectFit];
    [cell.leftLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleRightMargin];
    [cell.leftLabel sizeToFit];

    [cell.rightLabel setContentMode:UIViewContentModeScaleAspectFit];
    [cell.rightLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleRightMargin];
    [cell.rightLabel sizeToFit];
    [cell.rightLabel setFrame:CGRectMake(cell.frame.size.width - cell.rightLabel.frame.size.width - 8, 10, cell.rightLabel.frame.size.width,cell.rightLabel.frame.size.height)];
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{
    

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark Actions
- (void) goBack{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void) submitOrder{
    
    
}
- (void)showStatus{

    statusOfPreparationImageView.hidden = NO;
    totalBgImage.hidden = YES;
    submitButton.hidden = YES;
    submitButton.enabled = NO;
    
    CGRect tmprect = sheetTableView.frame;
    tmprect.size.height = sheetTableView.frame.size.height + 50;
    sheetTableView.frame = tmprect;
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithTitle:@"ЗВОНОК" style:UIBarButtonItemStyleBordered target:self action:@selector(makeCall)];
    [rightItem  setTitleTextAttributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:14.0] } forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = rightItem;
    self.navigationController.navigationBar.tintColor = [UIColor grayColor];
}
- (void) makeCall{
    
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
