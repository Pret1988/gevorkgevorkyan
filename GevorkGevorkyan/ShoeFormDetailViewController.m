//
//  ShoeFormDetailViewController.m
//  GevorkGevorkyan
//
//  Created by Garnik Ghazaryan on 11/20/14.
//  Copyright (c) 2014 Garnik Ghazaryan. All rights reserved.
//

#import "ShoeFormDetailViewController.h"
#import "Utils/Utils.h"
#import "PlatformHeightAdjustView.h"

@interface ShoeFormDetailViewController (){

    CGSize screenSize;
    UILabel *heelLabel;
    UILabel *platformLabel;
    UISwitch *heelPlatformSwitch;
    UIButton *finishButton;
    
    PlatformHeightAdjustView *platformAdjustView;
}

@end

@implementation ShoeFormDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    self.view.backgroundColor = rgba(250, 249, 245, 1);
    self.title = @"РЕДАКТОР";
    self.navigationController.navigationBar.backgroundColor = [UIColor whiteColor];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithTitle:@"ОТМЕНА" style:UIBarButtonItemStyleBordered target:self action:@selector(goBack)];
    [leftItem  setTitleTextAttributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:14] } forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem = leftItem;
    
//    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithTitle:@"ГОТОВО" style:UIBarButtonItemStyleBordered target:self action:@selector(finishEditing)];
//    [rightItem  setTitleTextAttributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:14.0] } forState:UIControlStateNormal];
//    self.navigationItem.rightBarButtonItem = rightItem;
    self.navigationController.navigationBar.tintColor = [UIColor grayColor];
    
    screenSize = [Utils getScreenBounds];
    [self uiConfigurations];
    
    // Do any additional setup after loading the view.
}

- (void) uiConfigurations{
    
    [Utils setViewToAddOn:self.view];
    heelPlatformSwitch = [Utils createSwitchWithRect:CGRectMake(screenSize.width/2 - 30, 50, 100, 40) target:self action:@selector(heelPlatformSwitch)];


    heelLabel = [Utils createLabelWithRect:CGRectMake(heelPlatformSwitch.frame.origin.x - 90, 45, 100, 40) title:@"КАБЛУК"];
    platformLabel = [Utils createLabelWithRect:CGRectMake(heelPlatformSwitch.frame.origin.x + heelPlatformSwitch.frame.size.width + 10, 45, 100, 40) title:@"ТАНКЕТКА"];
    finishButton = [Utils createButtonInRect:CGRectMake(0, screenSize.height - 50, screenSize.width, 50) title:@"" target:self action:@selector(finishEditing)  backGroundImage:[UIImage imageNamed:@"finishButtonPressed.png"] highlightImage:nil];
    platformAdjustView = [[PlatformHeightAdjustView alloc] initWithFrame:CGRectMake(0, heelPlatformSwitch.frame.origin.y + heelPlatformSwitch.frame.size.height + 10, screenSize.width, screenSize.height - (heelPlatformSwitch.frame.origin.y + heelPlatformSwitch.frame.size.height) - finishButton.frame.size.height - 22)];
    [self.view addSubview:platformAdjustView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Actions
- (void) goBack{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void) finishEditing{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void) heelPlatformSwitch{
    
    platformAdjustView.isSwitchOn = heelPlatformSwitch.isOn;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
