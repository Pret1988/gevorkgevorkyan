//
//  CustomCollectionViewCell.h
//  GevorkGevorkyan
//
//  Created by Garnik Ghazaryan on 10/20/14.
//  Copyright (c) 2014 Garnik Ghazaryan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CatalogViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *catalogCellImageView;
@property (strong, nonatomic) IBOutlet UILabel *catalogCellPriceLabel;

@end
