//
//  MainMenuView.m
//  GevorkGevorkyan
//
//  Created by Garnik Ghazaryan on 10/18/14.
//  Copyright (c) 2014 Garnik Ghazaryan. All rights reserved.
//

#import "MainMenuView.h"
#import "Utils.h"


@implementation MainMenuView {
    UIImageView *bgImageView;
    UIButton *catalogButton;
    UIButton *menuButton;
    UIButton *createButton;
    
    UILabel *firstLine;
    UILabel *secondLine;
    UILabel *titleLabel;
    
    CGSize scrSize;

}

@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        scrSize = [Utils getScreenBounds];
        [self uiConfigurations];
        
    }
    return self;
}

- (void) uiConfigurations {
    
    [Utils setViewToAddOn:self];
    bgImageView = [Utils createImageViewWithRect:self.bounds image:[UIImage imageNamed:@"bgImage.png"]];
    
    catalogButton = [Utils createButtonInRect:CGRectMake(scrSize.width - 15 - 70, 15, 70, 40) title:@"КАТАЛОГ" target:self action:@selector(catalogButtonPressed) backGroundImage:nil highlightImage:nil];
    menuButton = [Utils createButtonInRect:CGRectMake(15, 15, 70, 40) title:@"МЕНЮ" target:self action:@selector(menuButtonPressed) backGroundImage:nil highlightImage:nil];
    createButton = [Utils createButtonInRect:CGRectMake(self.frame.size.width/2 - 50, self.frame.size.height/2 + 70, 100, 33) title:@"" target:self action:@selector(createButtonPressed) backGroundImage:[UIImage imageNamed:@"createShoe.png"] highlightImage:nil];
    
    titleLabel = [Utils createLabelWithRect:CGRectMake(scrSize.width/2 - 70, 10, 140, 50) title:@"Gevorg Gevorgyan"];
    titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.numberOfLines = 2.0f;
    titleLabel.font = [UIFont fontWithName:@"GothamPro" size:20];
    titleLabel.textColor = rgba(231, 198, 128, 1);
    
    firstLine = [Utils createLabelWithRect:CGRectMake(self.frame.size.width/2 - 120, self.frame.size.height/2 - 70, 240, 50) title:@"СОЗДАЙ СВОЮ"];
    firstLine.textColor = [UIColor whiteColor];
    firstLine.textAlignment = NSTextAlignmentCenter;
    firstLine.font = [UIFont systemFontOfSize:28.0];
    
    secondLine = [Utils createLabelWithRect:CGRectMake(self.frame.size.width/2 - 120, self.frame.size.height/2 - 30, 240, 50) title:@"индеивидуальную обувь"];
    secondLine.textColor = [UIColor whiteColor];
    secondLine.textAlignment = NSTextAlignmentCenter;
    secondLine.font = [UIFont systemFontOfSize:16.0];
    
    [catalogButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [menuButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}


#pragma mark ACTIONS

-(void) catalogButtonPressed {
    [catalogButton setTitleColor:rgba(231, 198, 128, 1) forState:UIControlStateNormal];
    [menuButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [delegate didCatalogPressed];
}
-(void) menuButtonPressed {
    [catalogButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [menuButton setTitleColor:rgba(231, 198, 128, 1) forState:UIControlStateNormal];
    [delegate didMenuPressed];
}
-(void) createButtonPressed {
    [delegate didCreatePressed];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
