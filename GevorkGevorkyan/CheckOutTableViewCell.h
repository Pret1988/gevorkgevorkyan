//
//  CheckOutTableViewCell.h
//  GevorkGevorkyan
//
//  Created by Garnik Ghazaryan on 11/30/14.
//  Copyright (c) 2014 Garnik Ghazaryan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckOutTableViewCell : UITableViewCell
@property (strong, nonatomic) UILabel *leftLabel;
@property (strong, nonatomic) UILabel *rightLabel;
@property (strong, nonatomic) IBOutlet UIImageView *dotImageView;

@end
