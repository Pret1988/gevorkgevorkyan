//
//  AppDelegate.h
//  GevorkGevorkyan
//
//  Created by Garnik Ghazaryan on 10/14/14.
//  Copyright (c) 2014 Garnik Ghazaryan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

