//
//  FootSizeAdjustViewController.m
//  GevorkGevorkyan
//
//  Created by Garnik Ghazaryan on 12/5/14.
//  Copyright (c) 2014 Garnik Ghazaryan. All rights reserved.
//

#import "FootSizeAdjustViewController.h"
#import "MZFormSheetController.h"

@interface FootSizeAdjustViewController ()<UITextFieldDelegate>

@end

@implementation FootSizeAdjustViewController{
    
    NSArray *descriptionTextArray;
    NSArray *imageNameArray;

    NSString *imageName;
}


@synthesize delegate,selectedItemAtIndex;
@synthesize titleLabel,sizeTextField,descriptionLabel,bodyImageView;

- (void)viewDidLoad {
    [super viewDidLoad];
    descriptionTextArray = @[@"Длина стопы - измеряется по самым крайним точкам ноги.",
                             @"Полнота ноги - измеряется по самым широким точкам носочной части.",
                             @"Объем пальцев - измеряется в наиболее широких точках у основания пальцев.",
                             @"Обхват ноги - измеряется по косточке над щиколоткой.",
                             @"Обхват ноги у икры - измеряется в самой узкой части ноги в районе щиколотки.",
                             @"Высота ноги - измеряется в высоту от пятки до желаемой высоты."
                             ];
    imageNameArray = @[@"footDescrImage1.png",
                       @"footDescrImage2.png",
                       @"footDescrImage3.png",
                       @"footDescrImage3.png",
                       @"footDescrImage4.png",
                       @"footDescrImage5.png",
                       @"footDescrImage6.png"];
    // Do any additional setup after loading the view from its nib.
    sizeTextField.delegate = self;

}
-(void)viewWillAppear:(BOOL)animated{

    [self uiConfigurations];
}
- (void) uiConfigurations{
    
    imageName = [imageNameArray objectAtIndex:selectedItemAtIndex];
    bodyImageView.image = [UIImage imageNamed:imageName];
    descriptionLabel.text = [descriptionTextArray objectAtIndex:selectedItemAtIndex];
    titleLabel.text = [NSString stringWithFormat:@"ЗАМЕР %ld из 6",(long)selectedItemAtIndex+1];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)closeButton:(id)sender {
    [self mz_dismissFormSheetControllerAnimated:YES completionHandler:nil];
}
- (IBAction)questionMarkButton:(id)sender {
}
- (IBAction)finishButton:(id)sender {
    
    [self mz_dismissFormSheetControllerAnimated:YES completionHandler:^(MZFormSheetController *formSheetController) {
        [delegate didFinishSizeEditingWithText:sizeTextField.text];
    }];
  
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{


    
    return YES;
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{

    textField.text = [textField.text stringByReplacingOccurrencesOfString:@" см" withString:@""];
    return YES;

}
-(BOOL)textFieldShouldEndEditing:(UITextField *)textField{

    textField.text = [NSString stringWithFormat:@"%@ см",textField.text];
    return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
