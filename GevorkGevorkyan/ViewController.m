//
//  ViewController.m
//  GevorkGevorkyan
//
//  Created by Garnik Ghazaryan on 10/14/14.
//  Copyright (c) 2014 Garnik Ghazaryan. All rights reserved.
//

#import "ViewController.h"
#import "GGBackBarButtonItem.h"
#import "Utils.h"
#import "NewUserViewController.h"
#import "MyCabinetViewController.h"

@interface ViewController (){

    CGFloat screenWidth;
    CGFloat screenHeight;
    
    UILabel *mainBodyLabel;
    UILabel *secBodyLabel;
    UIImageView *bgImageView;
    UIImageView *loginBgView;
    UIImageView *passBgView;
    UIButton *enterButton;
    UIButton *lostPasswordButton;
    UITextField *loginTextField;
    UITextField *passTextField;
    
    NewUserViewController *newUserViewController;
    MyCabinetViewController *myCabinetVC;
}

@end

@implementation ViewController

@synthesize isSignedIn;

#pragma mark VIEW LYFECYCLE
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    screenWidth     = self.view.frame.size.width;
    screenHeight    = self.view.frame.size.height;

    self.title = @"АВТОРИЗАЦИЯ";
    //    self.navigationController.navigationBar.backItem.title = @"Назад";
    UIBarButtonItem *backButton = [[GGBackBarButtonItem alloc] initWithImage:[UIImage new]];
    backButton.title = @"Назад";
    self.navigationItem.backBarButtonItem = backButton;
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithTitle:@"МЕНЮ" style:UIBarButtonItemStyleBordered target:self action:@selector(openMenuPressed)];
    [leftItem  setTitleTextAttributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:14] } forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithTitle:@"НОВЫЙ" style:UIBarButtonItemStyleBordered target:self action:@selector(newContactPressed)];
    [rightItem  setTitleTextAttributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:14] } forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = rightItem;
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    [self uiConfigurations];
    
    myCabinetVC = [[MyCabinetViewController alloc] init];
}

- (void) uiConfigurations {
    
    [Utils setViewToAddOn:self.view];
    
    bgImageView = [Utils createImageViewWithRect:self.view.frame image:[UIImage imageNamed:@"loginBG.png"]];
    NSLog(@"size :%@",NSStringFromCGSize([Utils getTextBoundsSize:@"уникальной обуви" font:[UIFont systemFontOfSize:20.0]]));
    mainBodyLabel = [Utils createLabelWithRect:CGRectMake(screenWidth/2 - 113, 120, 226, 40) title:@"ВОЙДИ В МИР"];
    mainBodyLabel.textColor = [UIColor whiteColor];
    mainBodyLabel.textAlignment = NSTextAlignmentCenter;
    mainBodyLabel.font = [UIFont systemFontOfSize:28.0];
    
    secBodyLabel = [Utils createLabelWithRect:CGRectMake(screenWidth/2 - 84, mainBodyLabel.frame.origin.y + 30, 168, 40) title:@"уникальной обуви"];
    secBodyLabel.textColor = [UIColor whiteColor];
    secBodyLabel.textAlignment = NSTextAlignmentCenter;
    secBodyLabel.font = [UIFont systemFontOfSize:16.0];
    
    loginBgView = [Utils createImageViewWithRect:CGRectMake(screenWidth/2 - 140, mainBodyLabel.frame.origin.y + 120, 280, 39) image:[UIImage imageNamed:@"loginBgImage.png"]];
    passBgView = [Utils createImageViewWithRect:CGRectMake(screenWidth/2 - 140, mainBodyLabel.frame.origin.y + 170, 280, 39) image:[UIImage imageNamed:@"passBgImage.png"]];
    enterButton = [Utils createButtonInRect:CGRectMake(screenWidth/2 - 141, mainBodyLabel.frame.origin.y + 245, 283, 43) title:@"" target:self action:@selector(enterPressed) backGroundImage:[UIImage imageNamed:@"enterButtonImage.png"] highlightImage:nil];
    lostPasswordButton = [Utils createButtonInRect:CGRectMake(screenWidth/2 - 51, mainBodyLabel.frame.origin.y + 315, 103, 11) title:@"" target:self action:@selector(resetPasswordPressed) backGroundImage:[UIImage imageNamed:@"lostPassButtonImage.png"] highlightImage:nil];
    loginTextField = [Utils createTextFieldWithRect:CGRectMake(loginBgView.frame.origin.x + 40, loginBgView.frame.origin.y , loginBgView.frame.size.width - 30, loginBgView.frame.size.height) title:@"" placeHolderText:@""];
    loginTextField.textColor = [UIColor whiteColor];
    passTextField = [Utils createTextFieldWithRect:CGRectMake(passBgView.frame.origin.x + 40, passBgView.frame.origin.y , passBgView.frame.size.width - 50, passBgView.frame.size.height) title:@"" placeHolderText:@""];
    passTextField.textColor = [UIColor whiteColor];
    loginTextField.delegate = self;
    passTextField.delegate = self;
}
- (void)viewWillAppear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark Other Methods
- (void)keyboardWillShow:(NSNotification *)notification
{
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = -80.0f;  //set the -35.0f to your required value
        self.view.frame = f;
    }];
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = 0.0f;
        self.view.frame = f;
    }];
}

#pragma mark TextField Delegates 

-(BOOL)textFieldShouldReturn:(UITextField *)textField{

    [textField resignFirstResponder];
    return YES;
}

#pragma mark ACTION METHODS

- (void) openMenuPressed{
    isSignedIn = NO;
    [self.navigationController popViewControllerAnimated:YES];
}
- (void) enterPressed{
    isSignedIn = YES;
    if (isSignedIn == YES) {
        [self.navigationController pushViewController:myCabinetVC animated:YES];        
    }

}
- (void) resetPasswordPressed{

}

- (void) newContactPressed {
    newUserViewController = [[NewUserViewController alloc] init];
    [self.navigationController pushViewController:newUserViewController animated:YES];
    
}

@end
