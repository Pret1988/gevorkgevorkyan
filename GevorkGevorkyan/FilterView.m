//
//  FilterViewController.m
//  GevorkGevorkyan
//
//  Created by Garnik Ghazaryan on 10/23/14.
//  Copyright (c) 2014 Garnik Ghazaryan. All rights reserved.
//

#import "FilterView.h"
#import "ColorPickerView.h"
#import "Utils.h"

@interface FilterView ()

@end

@implementation FilterView {

    ColorPickerView *colorPickerView;
    UIButton *sizeButton;
    UIButton *materialButton;
    UIButton *applyButton;
    UIButton *resetButton;
    UILabel *sizeButtonLabel;
    UILabel *materialButtonLabel;
    
    
    UILabel *sizeLabel;
    UILabel *materialLabel;
    UILabel *colorLabel;
    UILabel *dottedLabel;
    
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self uiConfigurations];
    }
    return self;
}

- (void) uiConfigurations {
    
    
    UIImageView *bgImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 250)];
    bgImageView.backgroundColor = [UIColor clearColor];
    bgImageView.image = [UIImage imageNamed:@"filterBgimage.png"];
    [self addSubview:bgImageView];
    self.backgroundColor = [UIColor clearColor];
//    self.userInteractionEnabled = YES;
    [Utils setViewToAddOn:self];
    colorPickerView = [[ColorPickerView alloc] initWithFrame:CGRectMake(0, 20, 320, 50)];
    [colorPickerView enableHorizontalScroll];
    [colorPickerView setIsMultipleSelectionAllowed:YES];
    colorPickerView.colorItems = [[NSMutableArray alloc] initWithObjects:
                                  [UIColor redColor],
                                  [UIColor yellowColor],
                                  [UIColor greenColor],
                                  [UIColor blueColor],
                                  [UIColor grayColor],
                                  [UIColor lightGrayColor],
                                  [UIColor blackColor],
                                  [UIColor yellowColor],
                                  [UIColor greenColor],
                                  [UIColor blueColor],
                                  [UIColor greenColor],
                                  [UIColor blackColor],
                                  nil];
    
    [self addSubview:colorPickerView];
    [colorPickerView viewInitiated];
    colorLabel = [Utils createLabelWithRect:CGRectMake(10, 3, 60, 20) title:@"ЦВЕТ:"];
    colorLabel.font = [UIFont systemFontOfSize:13];
    sizeLabel = [Utils createLabelWithRect:CGRectMake(10, 80, 60, 20) title:@"РАЗМЕР:"];
    sizeLabel.font = [UIFont systemFontOfSize:13];
    materialLabel = [Utils createLabelWithRect:CGRectMake(100, 80, 80, 20) title:@"МАТЕРИАЛ:"];
    materialLabel.font = [UIFont systemFontOfSize:13];
    dottedLabel = [Utils createLabelWithRect:CGRectMake(3, 160, 320, 20) title:@"................................................................."];
    dottedLabel.textColor = [UIColor lightGrayColor];
    
    sizeButton = [Utils createButtonInRect:CGRectMake(10, 105, 80, 40) title:@"" target:self action:@selector(sizeButtonPressed) backGroundImage:[UIImage imageNamed:@"sizeButtonImage.png"] highlightImage:nil];
    sizeButtonLabel = [Utils createLabelWithRect:CGRectMake(15, 0, sizeButton.frame.size.width - 15, sizeButton.frame.size.height) title:@"37"];
    sizeButtonLabel.textAlignment = NSTextAlignmentLeft;
    [sizeButton addSubview:sizeButtonLabel];
    
    materialButton = [Utils createButtonInRect:CGRectMake(100, 105, 208, 40) title:@"" target:self action:@selector(matertialButtonPressed) backGroundImage:[UIImage imageNamed:@"materialButtonImage.png"] highlightImage:nil];
    materialButtonLabel = [Utils createLabelWithRect:CGRectMake(15, 0, sizeButton.frame.size.width - 15, sizeButton.frame.size.height) title:@"Rubber"];
    materialButtonLabel.textAlignment = NSTextAlignmentLeft;
    [materialButton addSubview:materialButtonLabel];
    
    applyButton = [Utils createButtonInRect:CGRectMake(167, 190, 142, 40) title:@"" target:self action:@selector(sizeButtonPressed) backGroundImage:[UIImage imageNamed:@"applyButtonImage.png"] highlightImage:nil];
    resetButton = [Utils createButtonInRect:CGRectMake(10, 190, 142, 40) title:@"" target:self action:@selector(resetFilters) backGroundImage:[UIImage imageNamed:@"resetButtonImage.png"] highlightImage:nil];
    
    //_menuDrop = [[SAMenuDropDown alloc] initWithWithSource:_btnSender menuHeight:300.0 itemNames:arrname itemImagesName:arrImg itemSubtitles:arrTitle];
    //_menuDrop.rowHeight = 20;
    
    _menuDrop.delegate = self;

}



- (void)saDropMenu:(SAMenuDropDown *)menuSender didClickedAtIndex:(NSInteger)buttonIndex
{
    
    NSLog(@"\n\n##<<%@>>##", menuSender);
    
    NSLog(@"\n\n\nClicked \n\n<<Index#%li>>", (long)buttonIndex);
}

#pragma mark Actions

- (void) sizeButtonPressed {
    
    NSMutableArray *arrname = [NSMutableArray array] ;
    for (int i=36; i <= 45; i++) {
        [arrname addObject:[NSString stringWithFormat:@"%d",i]];
    }
    [self showDropDownFromSender:sizeButton label:sizeButtonLabel andParameters:@[arrname]];
    
}
- (void) matertialButtonPressed{
    [self showDropDownFromSender:materialButton label:materialButtonLabel andParameters:@[@[@"Lather"]]];
}

- (void) resetFilters{
    [colorPickerView reloadData];
    sizeButtonLabel.text = @"37";
    materialButtonLabel.text = @"Rubber";
}

- (void) showDropDownFromSender:(UIButton*)sender label:(UILabel*)label andParameters:(NSArray*)arrayofItems{
    if(!sender.selected) {
        _menuDrop = [[SAMenuDropDown alloc] initWithSource:sender menuHeight:100.f itemName:arrayofItems[0]];
        _menuDrop.buttoTitleLabel = label;
        [_menuDrop showSADropDownMenuWithAnimation:kSAMenuDropAnimationDirectionBottom];
    }else {
        [_menuDrop hideSADropDownMenu];
    }
    sender.selected = !sender.selected;
    [_menuDrop menuItemSelectedBlock:^(SAMenuDropDown *menu, NSInteger index) {
        sender.selected = !sender.selected;
        NSLog(@"\n<<Block: Item = %li>>", (long)index);
    }];

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
