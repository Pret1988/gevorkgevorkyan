//
//  CatalogViewController.m
//  GevorkGevorkyan
//
//  Created by Garnik Ghazaryan on 10/20/14.
//  Copyright (c) 2014 Garnik Ghazaryan. All rights reserved.
//

#import "CatalogViewController.h"
#import "Utils.h"
#import "CatalogViewCell.h"
#import "GGBackBarButtonItem.h"
#import "ColorPickerView.h"
#import "FilterView.h"
#import "AKPickerView.h"
#import "ShoesViewController.h"

@interface CatalogViewController ()  <AKPickerViewDelegate>
@property (nonatomic, strong) AKPickerView *pickerView;
@property (nonatomic, strong) NSArray *filterTitles;
@end

@implementation CatalogViewController {
    FilterView *filter;
    ShoesViewController *shoesViewController;
    CGSize screenSize;
}

- (void)viewDidLoad {
    
    self.view = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    self.view.backgroundColor = rgba(250, 249, 245, 1);
    self.title = @"КАТАЛОГ";
    self.navigationController.navigationBar.backgroundColor = [UIColor whiteColor];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithTitle:@"НАЗАД" style:UIBarButtonItemStyleBordered target:self action:@selector(goBack)];
    [leftItem  setTitleTextAttributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:14] } forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithTitle:@"ФИЛЬТР" style:UIBarButtonItemStyleBordered target:self action:@selector(showFilter)];
    [rightItem  setTitleTextAttributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:14.0] } forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = rightItem;
    self.navigationController.navigationBar.tintColor = [UIColor grayColor];
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    _collectionView=[[UICollectionView alloc] initWithFrame:self.view.frame collectionViewLayout:layout];
    [_collectionView setDataSource:self];
    [_collectionView setDelegate:self];
    
//    [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    _collectionView.backgroundColor = rgba(247, 245, 239, 1);
    filter = [[FilterView alloc] initWithFrame:CGRectMake(0, -340, 320, 250)];
    [self.view addSubview:_collectionView];
    shoesViewController = [[ShoesViewController alloc] init];
    
    screenSize = [Utils getScreenBounds];
    [self configurePickerView];
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{

    [_collectionView registerNib:[UINib nibWithNibName:@"CatalogViewCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"myCellIdentifier"];
}
#pragma mark Collection view delegates;
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 15;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CatalogViewCell *cell = (CatalogViewCell*) [collectionView dequeueReusableCellWithReuseIdentifier:@"myCellIdentifier" forIndexPath:indexPath];
    cell.catalogCellImageView.image = [UIImage imageNamed:@"shoes.png"];
    cell.layer.borderWidth = 1.5f;
    cell.layer.cornerRadius = 5.f;
    cell.layer.borderColor = rgba(170, 178, 189, 1).CGColor;
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(100, 110);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 2.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 2.0;
}

// Layout: Set Edges
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    // return UIEdgeInsetsMake(0,8,0,8);  // top, left, bottom, right
    return UIEdgeInsetsMake(0,5,0,5);  // top, left, bottom, right
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {

    NSLog(@"Selected items # %ld",(long)indexPath.row);
    [self.navigationController pushViewController:shoesViewController animated:YES];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




- (void) configurePickerView {
    
    self.pickerView = [[AKPickerView alloc] initWithFrame:CGRectMake(-100, screenSize.height-60, screenSize.width + 200, 60)];
    self.pickerView.delegate = self;
    self.pickerView.highlightedTextColor = [UIColor colorWithRed:253/255. green:198/255. blue:128/255. alpha:1.];
    self.pickerView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.pickerView.backgroundColor = rgba(250, 250, 245, 1);
    [self.pickerView selectItem:2 animated:NO];
    [self.view addSubview:self.pickerView];
    UIImageView *dotImage = [[UIImageView alloc] initWithFrame:CGRectMake(self.pickerView.frame.size.width/2 - 2, 5, 6, 6)];
    dotImage.image = [UIImage imageNamed:@"selectedDot.png"];
    [self.pickerView addSubview:dotImage];
    self.pickerView.font = [UIFont systemFontOfSize:16.0];
    self.pickerView.highlightedFont = [UIFont systemFontOfSize:18.0];
    self.pickerView.interitemSpacing = 20.0;
    self.pickerView.fisheyeFactor = 0;
    
    self.filterTitles = @[@"САПОГИ",
                          @"ТУФЛИ",
                          @"НОВИНКИ",
                          @"БАЛЕТКИ",
                          @"БОТИНКИ",
                          @"Chiba",
                          @"Hyogo",
                          @"Hokkaido",
                          @"Fukuoka",
                          @"Shizuoka"];
    
    [self.pickerView reloadData];
    
}
#pragma mark AKPickerView Delegates

- (NSUInteger)numberOfItemsInPickerView:(AKPickerView *)pickerView
{
    return [self.filterTitles count];
}

- (NSString *)pickerView:(AKPickerView *)pickerView titleForItem:(NSInteger)item
{
    return self.filterTitles[item];
}

- (void)pickerView:(AKPickerView *)pickerView didSelectItem:(NSInteger)item
{
    NSLog(@"%@", self.filterTitles[item]);
    
}


#pragma mark Actions

- (void) showFilter{
    
    
    if (![filter isDescendantOfView:self.view]) {
        [self.view addSubview:filter];
        CGRect temp = filter.frame;
        temp.origin.y = 44 ;
        [UIView animateWithDuration:0.5
                              delay:0.0
                            options: UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             
                             filter.frame = temp;
                         }completion:^(BOOL finished){
                             
                         }];
        
    }else{
        CGRect temp = filter.frame;
        temp.origin.y = -340 ;
        [UIView animateWithDuration:0.5
                              delay:0.0
                            options: UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             filter.frame = temp;
                         }completion:^(BOOL finished){
                            [filter removeFromSuperview];
                         }];
        
    }

}
- (void) goBack{

    [self.navigationController popViewControllerAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
