//
//  GGBackBarButtonItem.h
//  GevorkGevorkyan
//
//  Created by Garnik Ghazaryan on 10/14/14.
//  Copyright (c) 2014 Garnik Ghazaryan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GGBackBarButtonItem : UIBarButtonItem{
    UIButton *button;
}
- (id)initWithImage:(UIImage *)image;
- (id)initWithImage:(UIImage *)image target:(id)target action:(SEL)action;


@end
