//
//  ColorPickerView.m
//  GevorkGevorkyan
//
//  Created by Garnik Ghazaryan on 10/22/14.
//  Copyright (c) 2014 Garnik Ghazaryan. All rights reserved.
//

#import "ColorPickerView.h"

@implementation ColorPickerView{
    UICollectionViewCell *cell;
    UICollectionViewFlowLayout *flowLayout;
   
    UIImageView *checkMarkView;
    NSMutableArray *selectedItems;

}
@synthesize colorItems,isMultipleSelectionAllowed,delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {

        flowLayout = [[UICollectionViewFlowLayout alloc] init];

        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height) collectionViewLayout:flowLayout];
        [_collectionView setDataSource:self];
        [_collectionView setDelegate:self];
        [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
        _collectionView.backgroundColor = [UIColor clearColor];
//        [_collectionView setAllowsSelection:YES];
//        [_collectionView setAllowsMultipleSelection:YES];
        [self addSubview:_collectionView];
        self.backgroundColor = [UIColor clearColor];
        selectedItems = [NSMutableArray array];
            }
    return self;
}
-(void)viewInitiated{

}
#pragma mark Collection view delegates;
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [colorItems count];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    cell = (UICollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    cell.backgroundColor = [UIColor clearColor];
    UIImageView *itemView = [self colorItemViewWithColor:colorItems[indexPath.row]];
    cell.tag = indexPath.row;
    [cell addSubview:itemView];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(42, 42);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 2.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 2.0;
}

// Layout: Set Edges
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    // return UIEdgeInsetsMake(0,8,0,8);  // top, left, bottom, right
    return UIEdgeInsetsMake(0,5,0,5);  // top, left, bottom, right
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {

    UICollectionViewCell *selectedCell = [collectionView cellForItemAtIndexPath:indexPath];
    UIColor *thClr = colorItems[indexPath.row];
    
    if (isMultipleSelectionAllowed) {
        BOOL isDeleted = FALSE;
        for (UIView *subView in selectedCell.subviews) {
            if (subView.tag == indexPath.row && CGRectEqualToRect(subView.frame, CGRectMake(10, 13, 22, 15))) {
                [subView removeFromSuperview];
                isDeleted = TRUE;
            }
        }
        NSLog(@"Selected items # %ld tag:%ld",(long)indexPath.row,[collectionView cellForItemAtIndexPath:indexPath].tag);
        if (!isDeleted) {
            checkMarkView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 13, 22, 15)];
            checkMarkView.image = [UIImage imageNamed:@"checkMarkImage.png"];
            checkMarkView.tag = indexPath.row;
            [selectedCell addSubview:checkMarkView];
        }
    }else {
        [delegate didSelectedColor:thClr];
    }
    
    const CGFloat* components = CGColorGetComponents(thClr.CGColor);
    NSLog(@"Color:Red: %f Green: %f Blue: %f Alpha:%f",components[0],components[1],components[2],CGColorGetAlpha(thClr.CGColor));
    

}


#pragma mark Cell View

- (UIImageView*)colorItemViewWithColor:(UIColor*)color {
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 42, 42)];
    imageView.backgroundColor = [UIColor clearColor];
    imageView.image = [UIImage imageNamed:@"colorItemBg.png"];
    //// Oval Drawing
    UIBezierPath* ovalPath = [UIBezierPath bezierPathWithOvalInRect: CGRectMake(5, 5, 32, 32)];
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    shapeLayer.fillRule = kCAFillRuleEvenOdd;
    shapeLayer.fillColor = color.CGColor;
    shapeLayer.path = [ovalPath CGPath];
    
    [imageView.layer addSublayer:shapeLayer];
    
    return imageView;
}
-(void)enableHorizontalScroll{

    [flowLayout setItemSize:CGSizeMake(191, 160)];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    flowLayout.minimumInteritemSpacing = 0.0f;
    _collectionView.showsHorizontalScrollIndicator = NO;
    
}
-(void)reloadData{
    [_collectionView reloadData];
}

-(void)setIsMultipleSelectionAllowed:(BOOL)multipleSelectionAllowed{
    if (multipleSelectionAllowed) {
        isMultipleSelectionAllowed = YES;
    }else {
        isMultipleSelectionAllowed = NO;
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {

 
}
*/

@end
