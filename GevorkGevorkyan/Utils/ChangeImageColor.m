//
//  ChangeImageColor.m
//  GevorkGevorkyan
//
//  Created by Garnik Ghazaryan on 12/4/14.
//  Copyright (c) 2014 Garnik Ghazaryan. All rights reserved.
//
// frameworks
#import <QuartzCore/QuartzCore.h>
#import "ChangeImageColor.h"

@interface ChangeImageColor()
typedef unsigned char byte;
#define Clamp255(a) (a>255 ? 255 : a)
@property (nonatomic, assign) NSInteger bytesPerRow;
@end

@implementation ChangeImageColor





+ (UIImage*) fromImage:(UIImage*)source toColourR:(UIColor*)color {
    
    // Thanks: http://brandontreb.com/image-manipulation-retrieving-and-updating-pixel-values-for-a-uiimage/ http://www.splinter.com.au/image-manipulation-pixel-by-pixel-in-objectiv/
    
    CGContextRef ctx;
    CGImageRef imageRef = [source CGImage];
    NSUInteger width = CGImageGetWidth(imageRef);
    NSUInteger height = CGImageGetHeight(imageRef);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    Byte *rawData = malloc(height * width * 4);
    NSUInteger bytesPerPixel = 4;
    NSUInteger bytesPerRow = bytesPerPixel * width;
    NSUInteger bitsPerComponent = 8;
    CGContextRef context = CGBitmapContextCreate(rawData, width, height,
                                                 bitsPerComponent, bytesPerRow, colorSpace,
                                                 kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageRef);
    CGContextRelease(context);
    
    CGColorRef myColorRef = color.CGColor;
    const CGFloat *colorComponents = CGColorGetComponents(myColorRef);
    
    int byteIndex = 0;
    for (int ii = 0 ; ii < width * height ; ++ii)
    {
//
//        NSLog(@"r=%hhu, g=%hhu, b=%hhu, a=%hhu", rawData[byteIndex], rawData[byteIndex + 1], rawData[byteIndex + 2], rawData[byteIndex + 3]);
        if ( (rawData[byteIndex] ==  255 && rawData[byteIndex+1] == 255 && rawData[byteIndex+2] == 255 && rawData[byteIndex+3] == 255 ) || (rawData[byteIndex] ==  0 && rawData[byteIndex+1] == 0 && rawData[byteIndex+2] == 0 && rawData[byteIndex+3] == 255 )) {
            if (CGColorGetNumberOfComponents(myColorRef) == 2) {
                rawData[byteIndex] =  colorComponents[0]*255.0;
                rawData[byteIndex+1] = colorComponents[0]*255.0;
                rawData[byteIndex+2] = colorComponents[0]*255.0;
                rawData[byteIndex+3] = 255;//colorComponents[1]*255.0;
            }
            else if (CGColorGetNumberOfComponents(myColorRef) == 4) {
                rawData[byteIndex] = colorComponents[0]*255.0;
                rawData[byteIndex+1] = colorComponents[1]*255.0;
                rawData[byteIndex+2] = colorComponents[2]*255.0;
                rawData[byteIndex+3] = 255;//colorComponents[3]*255.0;
                
            }
            
        }
//        NSLog(@"Modified r=%hhu, g=%hhu, b=%hhu, a=%hhu", rawData[byteIndex], rawData[byteIndex + 1], rawData[byteIndex + 2], rawData[byteIndex + 3]);
//        NSLog(@"r=%f, g=%f, b=%f, a=%f", colorComponents[0], colorComponents[1], colorComponents[2], colorComponents[3]);
        
        byteIndex += 4;
    }
    
    ctx = CGBitmapContextCreate(rawData,
                                CGImageGetWidth( imageRef ),
                                CGImageGetHeight( imageRef ),
                                8,
                                bytesPerRow,
                                colorSpace,
                                kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CGColorSpaceRelease(colorSpace);
    
    imageRef = CGBitmapContextCreateImage (ctx);
    UIImage* rawImage = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
//    UIImageWriteToSavedPhotosAlbum(rawImage, nil, nil, nil);

    CGContextRelease(ctx);
    free(rawData);
    
    return rawImage;
}
+ (UIImage*) fromImage:(UIImage*)source currentColor:(UIColor *) currentColor newColor:(UIColor*)newColor {
    
    // Thanks: http://brandontreb.com/image-manipulation-retrieving-and-updating-pixel-values-for-a-uiimage/ http://www.splinter.com.au/image-manipulation-pixel-by-pixel-in-objectiv/
    
    CGContextRef ctx;
    CGImageRef imageRef = [source CGImage];
    NSUInteger width = CGImageGetWidth(imageRef);
    NSUInteger height = CGImageGetHeight(imageRef);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    Byte *rawData = malloc(height * width * 4);
    NSUInteger bytesPerPixel = 4;
    NSUInteger bytesPerRow = bytesPerPixel * width;
    NSUInteger bitsPerComponent = 8;
    CGContextRef context = CGBitmapContextCreate(rawData, width, height,
                                                 bitsPerComponent, bytesPerRow, colorSpace,
                                                 kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageRef);
    CGContextRelease(context);
    
    CGColorRef currentColorRef = currentColor.CGColor;
    const CGFloat *currentColorComponents = CGColorGetComponents(currentColorRef);

    CGColorRef newColorRef = newColor.CGColor;
    const CGFloat *newColorComponents = CGColorGetComponents(newColorRef);

    
    int byteIndex = 0;
    for (int ii = 0 ; ii < width * height ; ++ii)
    {
        //( (rawData[byteIndex] ==  currentColorComponents[0]*255 && rawData[byteIndex+1] == currentColorComponents[1]*255 && rawData[byteIndex+2] == currentColorComponents[2]*255 && rawData[byteIndex+3] == currentColorComponents[3]*255 ) || (rawData[byteIndex] ==  0 && rawData[byteIndex+1] == 0 && rawData[byteIndex+2] == 0 && rawData[byteIndex+3] == 255 ))
//        NSLog(@"raw data r=%hhu, g=%hhu, b=%hhu, a=%hhu", rawData[byteIndex], rawData[byteIndex + 1], rawData[byteIndex + 2], rawData[byteIndex + 3]);
//        NSLog(@"r=%.1f, g=%.1f, b=%.1f, a=%.1f", currentColorComponents[0]*255, currentColorComponents[1]*255, currentColorComponents[2]*255, currentColorComponents[3]*255);
        if (rawData[byteIndex+3] > 0.3*255 && (rawData[byteIndex] !=  0 && rawData[byteIndex+1] != 0 && rawData[byteIndex+2] != 0 && rawData[byteIndex+3] != 0) ) {
            if (CGColorGetNumberOfComponents(newColorRef) == 2) {
                rawData[byteIndex] =  newColorComponents[0]*255.0;
                rawData[byteIndex+1] = newColorComponents[0]*255.0;
                rawData[byteIndex+2] = newColorComponents[0]*255.0;
                rawData[byteIndex+3] = newColorComponents[1]*255.0;
            }
            else if (CGColorGetNumberOfComponents(newColorRef) == 4) {
                rawData[byteIndex] = newColorComponents[0]*255.0;
                rawData[byteIndex+1] = newColorComponents[1]*255.0;
                rawData[byteIndex+2] = newColorComponents[2]*255.0;
                rawData[byteIndex+3] = newColorComponents[3]*255.0;
                
            }
            
        }
        //        NSLog(@"Modified r=%hhu, g=%hhu, b=%hhu, a=%hhu", rawData[byteIndex], rawData[byteIndex + 1], rawData[byteIndex + 2], rawData[byteIndex + 3]);
        //        NSLog(@"r=%f, g=%f, b=%f, a=%f", colorComponents[0], colorComponents[1], colorComponents[2], colorComponents[3]);
        
        byteIndex += 4;
    }
    
    ctx = CGBitmapContextCreate(rawData,
                                CGImageGetWidth( imageRef ),
                                CGImageGetHeight( imageRef ),
                                8,
                                bytesPerRow,
                                colorSpace,
                                kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CGColorSpaceRelease(colorSpace);
    
    imageRef = CGBitmapContextCreateImage (ctx);
    UIImage* rawImage = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    //    UIImageWriteToSavedPhotosAlbum(rawImage, nil, nil, nil);
    
    CGContextRelease(ctx);
    free(rawData);
    
    return rawImage;
}

@end
