//
//  GGBackBarButtonItem.m
//  GevorkGevorkyan
//
//  Created by Garnik Ghazaryan on 10/14/14.
//  Copyright (c) 2014 Garnik Ghazaryan. All rights reserved.
//

#import "GGBackBarButtonItem.h"

@implementation GGBackBarButtonItem

- (id)initWithImage:(UIImage *)image
{
    button = [UIButton buttonWithType:UIButtonTypeCustom];
    self = [super initWithCustomView:button];
    if (self) {
        CGFloat buttonWidth = image.size.width;
        CGFloat buttonHeight = image.size.height;
        button.frame = CGRectMake(0, 0, buttonWidth, buttonHeight);
        self.width = buttonWidth;
        
        // Normal state background
        UIImage *backgroundImage = image;
        [button setBackgroundImage:backgroundImage forState:UIControlStateNormal];
        
        // Pressed state background
        backgroundImage = image;
        [button setBackgroundImage:backgroundImage forState:UIControlStateHighlighted];
    }
    return self;
}

- (id)initWithImage:(UIImage *)image target:(id)target action:(SEL)action
{
    button = [UIButton buttonWithType:UIButtonTypeCustom];
    self = [super initWithCustomView:button];
    if (self) {
        self.target = target;
        self.action = action;
        
        CGFloat buttonWidth = image.size.width;
        CGFloat buttonHeight = image.size.height;
        button.frame = CGRectMake(0, 0, buttonWidth, buttonHeight);
        self.width = buttonWidth;
        
        // Normal state background
        UIImage *backgroundImage = image;
        [button setBackgroundImage:backgroundImage forState:UIControlStateNormal];
        
        // Pressed state background
        backgroundImage = image;
        [button setBackgroundImage:backgroundImage forState:UIControlStateHighlighted];
        [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}


@end
