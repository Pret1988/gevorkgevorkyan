//
//  ColorPickerView.h
//  GevorkGevorkyan
//
//  Created by Garnik Ghazaryan on 10/22/14.
//  Copyright (c) 2014 Garnik Ghazaryan. All rights reserved.
//



#import <UIKit/UIKit.h>

@protocol ColorPickerViewDelegate <NSObject>

@optional - (void)didSelectedColor:(UIColor*)color;
@optional - (NSArray*)didSelectedColors:(NSArray*)colors;

@end

@interface ColorPickerView : UIView<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate>
{
    UICollectionView *_collectionView;
}
@property (nonatomic, retain) NSMutableArray *colorItems;
@property (nonatomic) BOOL isMultipleSelectionAllowed;
@property (nonatomic) id delegate;
-(void)viewInitiated;
-(void)enableHorizontalScroll;
-(void)reloadData;
-(void)setIsMultipleSelectionAllowed:(BOOL)multipleSelectionAllowed;
@end
