//
//  ChangeImageColor.h
//  GevorkGevorkyan
//
//  Created by Garnik Ghazaryan on 12/4/14.
//  Copyright (c) 2014 Garnik Ghazaryan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ChangeImageColor : UIView

+ (UIImage *)switchColorOfImagePixels:(UIImage*)image with:(UIColor*)color;
+ (UIImage*) fromImage:(UIImage*)source toColourR:(UIColor*)color;
+ (UIImage*) fromImage:(UIImage*)source currentColor:(UIColor *) currentColor newColor:(UIColor*)newColor;
@end
