//
//  NewUserViewController.m
//  GevorkGevorkyan
//
//  Created by Garnik Ghazaryan on 10/13/14.
//  Copyright (c) 2014 Garnik Ghazaryan. All rights reserved.
//

#import "NewUserViewController.h"
#import "GGBackBarButtonItem.h"
#import "Utils.h"

@interface NewUserViewController ()

@end

@implementation NewUserViewController {

    CGFloat screenWidth;
    CGFloat screenHeight;
    NSDateFormatter *df;
    
    UIImageView *profileImageView;
    UIScrollView *bgScrollView;
    UIButton *choosePhotoButton;
    UIButton *takePhotoButton;
    
    UILabel *nameLabel;
    UILabel *lastNameLabel;
    UILabel *sexLabel;
    UILabel *birthDateLabel;
    UILabel *phoneNumLabel;
    UILabel *emailLabel;
    UILabel *passwordLabel;
    UILabel *confirmPasswordLabel;
    
    UITextField *nameField;
    UITextField *lastNameField;
    UISegmentedControl *sexControl;
    UIDatePicker *birthDatePicker;
    UITextField *birthDateField;
    UITextField *phoneNumField;
    UITextField *emailField;
    UITextField *passwordField;
    UITextField *confirmPasswordField;
    
    UIImagePickerController *picker;

    CGFloat currentOffset;
}



- (void) uiConfigurations {

    bgScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 60, self.view.frame.size.width, self.view.frame.size.height)];
    bgScrollView.contentSize = CGSizeMake(self.view.frame.size.width, 600);
    bgScrollView.bounces = NO;
    profileImageView = [Utils createImageViewWithRect:CGRectMake(0, -60, 320, 200) image:nil];
    profileImageView.image = [UIImage imageNamed:@"placeHolderImage.png"];
    profileImageView.contentMode = UIViewContentModeScaleToFill;
    df = [[NSDateFormatter alloc]init];
    [df setDateFormat:@"dd/MM/yyyy"];

    
    UIFont *labelsFont = [UIFont systemFontOfSize:10.0];
    
    nameLabel               = [Utils createLabelWithRect:CGRectMake(10, 150, 50, 30) title:@"ИМЯ:"];
    lastNameLabel           = [Utils createLabelWithRect:CGRectMake(screenWidth/2, 150, 150, 30) title:@"ФАМИЛИЯ:"];
    sexLabel                = [Utils createLabelWithRect:CGRectMake(10, 220, 50, 30) title:@"ПОЛ:"];
    birthDateLabel          = [Utils createLabelWithRect:CGRectMake(screenWidth/2, 220, 150, 30) title:@"ДАТА РОЖДЕНИЯ:"];
    phoneNumLabel           = [Utils createLabelWithRect:CGRectMake(10, 300, 150, 30) title:@"ТЕЛЕФОН:"];
    emailLabel              = [Utils createLabelWithRect:CGRectMake(10, 370, 50, 30) title:@"EMAIL"];
    passwordLabel           = [Utils createLabelWithRect:CGRectMake(10, 440, 50, 30) title:@"ПАРОЛЬ:"];
    confirmPasswordLabel    = [Utils createLabelWithRect:CGRectMake(screenWidth/2, 440, 250, 30) title:@"ПОВТОРИТЕ ПАРОЛЬ:"];
    
    nameLabel.font = labelsFont;
    lastNameLabel.font = labelsFont;
    sexLabel.font = labelsFont;
    birthDateLabel.font = labelsFont;
    phoneNumLabel.font = labelsFont;
    emailLabel.font = labelsFont;
    passwordLabel.font = labelsFont;
    confirmPasswordLabel.font = labelsFont;
    
    nameField               = [Utils createTextFieldWithRect:CGRectMake(10, 180, screenWidth/2 - 20, 40) title:@"" placeHolderText:@"ИМЯ"];
    lastNameField           = [Utils createTextFieldWithRect:CGRectMake(screenWidth/2, 180, screenWidth/2 - 20, 40) title:@"" placeHolderText:@"ФАМИЛИЯ"];
    sexControl              = [[UISegmentedControl alloc] initWithItems:@[@"ГОСПОДИН",@"ГОСПОЖА"]];
    birthDateField          = [Utils createTextFieldWithRect:CGRectMake(screenWidth/2, 250, screenWidth/2 - 20, 40) title:@"" placeHolderText:[NSString stringWithFormat:@"%@",[df stringFromDate:[NSDate date]]]];
    phoneNumField           = [Utils createTextFieldWithRect:CGRectMake(10, 330, screenWidth - 20, 40) title:@"" placeHolderText:@"ТЕЛЕФОН"];
    emailField              = [Utils createTextFieldWithRect:CGRectMake(10, 400, screenWidth - 20, 40) title:@"" placeHolderText:@"EMAIL@DOMAIN.NAME"];
    passwordField           = [Utils createTextFieldWithRect:CGRectMake(10, 470, screenWidth/2 - 20, 40) title:@"" placeHolderText:@"ПАРОЛЬ"];
    confirmPasswordField    = [Utils createTextFieldWithRect:CGRectMake(screenWidth/2, 470, screenWidth/2 - 20, 40) title:@"" placeHolderText:@"ПОВТОРИТЕ ПАРОЛЬ"];
    
    nameField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    lastNameField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    birthDateField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    phoneNumField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    emailField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    passwordField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    confirmPasswordField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    
    nameField.delegate = self;
    lastNameField.delegate = self;
    birthDateField.delegate = self;
    phoneNumField.delegate = self;
    emailField.delegate = self;
    passwordField.delegate = self;
    confirmPasswordField.delegate = self;

    nameField.background            = [UIImage imageNamed:@"textFieldBg.png"];
    lastNameField.background        = [UIImage imageNamed:@"textFieldBg.png"];
    birthDateField.background       = [UIImage imageNamed:@"textFieldBg.png"];
    phoneNumField.background        = [UIImage imageNamed:@"textFieldBg.png"];
    emailField.background           = [UIImage imageNamed:@"textFieldBg.png"];
    passwordField.background        = [UIImage imageNamed:@"textFieldBg.png"];
    confirmPasswordField.background = [UIImage imageNamed:@"textFieldBg.png"];
    [sexControl setFrame:CGRectMake(10, 250, screenWidth/2 - 20, 40)];
    sexControl.tintColor = rgba(229, 193, 121, 1);
    [sexControl setTitleTextAttributes:@{NSForegroundColorAttributeName:rgba(78, 60, 54, 1), NSFontAttributeName:[UIFont systemFontOfSize:8.0]} forState:UIControlStateNormal];
    [sexControl setContentPositionAdjustment:UIOffsetMake(0, 1) forSegmentType:UISegmentedControlSegmentAny barMetrics:UIBarMetricsDefault];
    
    birthDatePicker = [[UIDatePicker alloc]init];
    birthDateField.inputView = birthDatePicker;
    birthDatePicker.datePickerMode = UIDatePickerModeDate;
    birthDatePicker.hidden = NO;
    birthDatePicker.date = [NSDate date];
    [birthDatePicker addTarget:self action:@selector(changeDate:) forControlEvents:UIControlEventValueChanged];
    
    choosePhotoButton = [Utils createButtonInRect:CGRectMake(15, 100, 24, 20) title:nil target:self action:@selector(choosePhoto) backGroundImage:[UIImage imageNamed:@"choosePhotoIcon.png"] highlightImage:nil];
    takePhotoButton   = [Utils createButtonInRect:CGRectMake(screenWidth - 35, 100, 24, 20) title:nil target:self action:@selector(takePhoto) backGroundImage:[UIImage imageNamed:@"takePhotoIcon.png"] highlightImage:nil];
    
    [self.view addSubview:bgScrollView];
    [bgScrollView addSubview:profileImageView];
    [bgScrollView addSubview:nameLabel];
    [bgScrollView addSubview:lastNameLabel];
    [bgScrollView addSubview:sexLabel];
    [bgScrollView addSubview:birthDateLabel];
    [bgScrollView addSubview:phoneNumLabel];
    [bgScrollView addSubview:emailLabel];
    [bgScrollView addSubview:passwordLabel];
    [bgScrollView addSubview:confirmPasswordLabel];
    [bgScrollView addSubview:nameField];
    [bgScrollView addSubview:lastNameField];
    [bgScrollView addSubview:sexControl];
    [bgScrollView addSubview:birthDateField];
    [bgScrollView addSubview:phoneNumField];
    [bgScrollView addSubview:emailField];
    [bgScrollView addSubview:passwordField];
    [bgScrollView addSubview:confirmPasswordField];
    [bgScrollView addSubview:choosePhotoButton];
    [bgScrollView addSubview:takePhotoButton];
    
    
}
#pragma mark VIEW LYFECYCLE
- (void)viewDidLoad {
    [super viewDidLoad];
    
    screenWidth     = self.view.frame.size.width;
    screenHeight    = self.view.frame.size.height;
    
    self.title = @"РЕГИСТРАЦИЯ";
    //    self.navigationController.navigationBar.backItem.title = @"Назад";
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithTitle:@"НАЗАД" style:UIBarButtonItemStyleBordered target:self action:@selector(goBack)];
    [leftItem  setTitleTextAttributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:14] } forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithTitle:@"СОЗДАТЬ" style:UIBarButtonItemStyleBordered target:self action:@selector(createNewUserPressed)];
    [rightItem  setTitleTextAttributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:14.0] } forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = rightItem;
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationController.navigationBar.tintColor = [UIColor grayColor];
    // Do any additional setup after loading the view.
    [self uiConfigurations];
    

}
- (void)viewWillAppear:(BOOL)animated {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];

//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow::) name:UIKeyboardWillShowNotification object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide::) name:UIKeyboardWillHideNotification object:nil];

}
-(void)viewDidAppear:(BOOL)animated{
    picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
}

- (void)viewWillDisappear:(BOOL)animated {
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark Other Methods
- (void)keyboardWillShow:(NSNotification *)notification andView:(UIView*) view
{
    CGRect tmpRect = [view convertRect:view.bounds toView:self.view];
    currentOffset = bgScrollView.contentOffset.y;
    
    [UIView animateWithDuration:0.3 animations:^{
        if (tmpRect.origin.y > self.view.frame.size.height - 250) {
            [bgScrollView setContentOffset:CGPointMake(0, currentOffset + (tmpRect.origin.y - 264) + tmpRect.size.height + 15) animated:YES];
        }
    }];
}

-(void)keyboardWillHide:(NSNotification *)notification andView:(UIView*) view
{
    [UIView animateWithDuration:0.3 animations:^{
        [bgScrollView setContentOffset:CGPointMake(0, currentOffset) animated:YES];
        [bgScrollView setContentSize:CGSizeMake(screenWidth, 600)];
    }];
}


#pragma mark ACTION METHODS

- (void) goBack{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void) createNewUserPressed{


}
- (void) changeDate:(id)sender{
    
    NSString *date = [NSString stringWithFormat:@"%@",[df stringFromDate:birthDatePicker.date]];
    NSLog(@"%@",[NSString stringWithFormat:@"%@",[df stringFromDate:birthDatePicker.date]]);
    birthDateField.text = date;
    [birthDateField resignFirstResponder];
}

- (void) choosePhoto {
    
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:nil];

}
- (void) takePhoto {
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:picker animated:YES completion:nil];
}
#pragma mark TextField Delegates

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    [self keyboardWillShow:nil andView:textField];
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    [self keyboardWillHide:nil andView:textField];
}
#pragma mark Other Delegates 

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    [self dismissViewControllerAnimated:YES completion:^{
        UIImage * img = [info valueForKey:UIImagePickerControllerOriginalImage];
        profileImageView.image = img;
    }];
    
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
