//
//  SettingsViewController.h
//  GevorkGevorkyan
//
//  Created by Garnik Ghazaryan on 12/2/14.
//  Copyright (c) 2014 Garnik Ghazaryan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>
#import <Accounts/Accounts.h>

@interface SettingsViewController : UIViewController {
    SLComposeViewController *slComposerSheet;
}

@end
