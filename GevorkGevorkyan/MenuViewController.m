//
//  MenuViewController.m
//  GevorkGevorkyan
//
//  Created by Garnik Ghazaryan on 10/17/14.
//  Copyright (c) 2014 Garnik Ghazaryan. All rights reserved.
//

#import "MenuViewController.h"
#import "Utils.h"
#import "GGBackBarButtonItem.h"
#import "AKPickerView.h"
#import "CatalogViewController.h"
#import "WorkroomViewController.h"

#import "MyOrdersViewController.h"
#import "SettingsViewController.h"
#import "ViewController.h"

@interface MenuViewController () <AKPickerViewDelegate>
@property (nonatomic, strong) AKPickerView *pickerView;
@property (nonatomic, strong) NSArray *filterTitles;
@end

@implementation MenuViewController {

    UITableView *menuTableView;
    MainMenuView *slidingView;
    iCarousel *itemsView;
    UIView *bottomSliderView;
    
    CGSize screenSize;
    
    NSArray *titleItemsArray;
    BOOL animationDirection;
    
    int numberOfOrders;
    
    WorkroomViewController *workRoomVC;
    MyOrdersViewController *myOrderVC;
    SettingsViewController *settingsVC;
    ViewController *authVC;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"ГЛАВНАЯ";
    self.view.backgroundColor = rgba(250, 249, 245, 1);
    UIBarButtonItem *leftBarButton = [[GGBackBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"infoIcon.png"] target:self action:@selector(infoPressed)];
    self.navigationItem.leftBarButtonItem = leftBarButton;

    UIBarButtonItem *rightBarButton = [[GGBackBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"settingsicon.png"] target:self action:@selector(settingsPressed)];
    self.navigationItem.rightBarButtonItem = rightBarButton;
    self.navigationController.navigationBar.backgroundColor = [UIColor whiteColor];
    screenSize = [Utils getScreenBounds];
    numberOfOrders = 2;
    [self uiConfigurations];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{

    if (![slidingView isDescendantOfView:self.view] || [slidingView isDescendantOfView:self.navigationController.view] ) {
        [self.navigationController.view addSubview:slidingView];
    }
}
-(void)viewWillDisappear:(BOOL)animated {
    [slidingView removeFromSuperview];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) uiConfigurations {
    
    animationDirection = TRUE;
    titleItemsArray = @[@"ГЛАВНАЯ",@"КАТАЛОГ",@"МАСТЕРСКАЯ",@"СЕРТИФИКАТЫ",[NSString stringWithFormat:@"МОИ ЗАКАЗЫ: %d",numberOfOrders],@"МОЙ КАБИНЕТ"];
    
    menuTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, screenSize.width, screenSize.height/2)];
    menuTableView.delegate = self;
    menuTableView.dataSource = self;
//    menuTableView.hidden = YES;
    
    itemsView = [[iCarousel alloc] initWithFrame:CGRectMake(0, screenSize.height/2, screenSize.width, screenSize.height/2 - 60)];
    itemsView.backgroundColor = [UIColor clearColor];
    itemsView.delegate = self;
    itemsView.dataSource = self;
    itemsView.type=iCarouselTypeLinear;
    itemsView.currentItemIndex = 1;

    itemsView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    itemsView.layer.borderWidth = 1.f;
//    bottomSliderView = [[UIView alloc] initWithFrame:CGRectMake(0, screenSize.height-60, screenSize.width, 60)];
//    bottomSliderView.backgroundColor = [UIColor whiteColor];
    
    slidingView = [[MainMenuView alloc] initWithFrame:CGRectMake(0, 0, screenSize.width, screenSize.height/2)];
    slidingView.backgroundColor = [UIColor clearColor];
    slidingView.delegate = self;
    
    [self.view addSubview:menuTableView];
    [self.view addSubview:itemsView];

    [self configurePickerView];
    
    workRoomVC = [[WorkroomViewController alloc] init];
    myOrderVC = [[MyOrdersViewController alloc] init];
    settingsVC = [[SettingsViewController alloc] init];
    authVC = [[ViewController alloc] init];
}

- (void) configurePickerView {
    
    self.pickerView = [[AKPickerView alloc] initWithFrame:CGRectMake(-100, screenSize.height-60, screenSize.width + 200, 60)];
    self.pickerView.delegate = self;
    self.pickerView.highlightedTextColor = [UIColor colorWithRed:253/255. green:198/255. blue:128/255. alpha:1.];
    self.pickerView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.pickerView selectItem:2 animated:NO];
    [self.view addSubview:self.pickerView];
    UIImageView *dotImage = [[UIImageView alloc] initWithFrame:CGRectMake(self.pickerView.frame.size.width/2 - 2, 5, 6, 6)];
    dotImage.image = [UIImage imageNamed:@"selectedDot.png"];
    [self.pickerView addSubview:dotImage];
    self.pickerView.font = [UIFont systemFontOfSize:16.0];
    self.pickerView.highlightedFont = [UIFont systemFontOfSize:18.0];
    self.pickerView.interitemSpacing = 20.0;
    self.pickerView.fisheyeFactor = 0;
    
    self.filterTitles = @[@"САПОГИ",
                    @"ТУФЛИ",
                    @"НОВИНКИ",
                    @"БАЛЕТКИ",
                    @"БОТИНКИ",
                    @"Chiba",
                    @"Hyogo",
                    @"Hokkaido",
                    @"Fukuoka",
                    @"Shizuoka"];
    
    [self.pickerView reloadData];

}
#pragma mark AKPickerView Delegates

- (NSUInteger)numberOfItemsInPickerView:(AKPickerView *)pickerView
{
    return [self.filterTitles count];
}

- (NSString *)pickerView:(AKPickerView *)pickerView titleForItem:(NSInteger)item
{
    return self.filterTitles[item];
}

- (void)pickerView:(AKPickerView *)pickerView didSelectItem:(NSInteger)item
{
    NSLog(@"%@", self.filterTitles[item]);
    
}


#pragma mark iCarousel
- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    return 15;
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view
{
    UILabel *descrLabel = nil;
    UILabel *priceLabel = nil;
    UIImageView *itemImage = nil;
    
    //create new view if no view is available for recycling
    if (view == nil)
    {
        view = [[[NSBundle mainBundle] loadNibNamed:@"ItemView" owner:self options:nil] lastObject];
        view.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, itemsView.frame.size.height - 10);
        view.layer.borderColor = [UIColor lightGrayColor].CGColor;
        view.layer.borderWidth = 1.5f;
        view.layer.cornerRadius = 5.f;
        
        descrLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, view.frame.size.width - 5, view.frame.size.height/3)];
        descrLabel.numberOfLines = 5;
        descrLabel.font = [UIFont systemFontOfSize:10.0];
        itemImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, view.frame.size.width/3 + 10, view.frame.size.height/3, view.frame.size.height/3)];
        itemImage.image = [UIImage imageNamed:@"shoes.png"];
        priceLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 2*view.frame.size.height/3, view.frame.size.width, view.frame.size.height/3)];
        priceLabel.numberOfLines = 5;
        priceLabel.textAlignment = NSTextAlignmentCenter;
    
        descrLabel.text = @"PUMPS IN REDDISH PURPLE METAL PATENT LEATHER";
        priceLabel.text = @"13 520 Р";
        
        [view addSubview:descrLabel];
        [view addSubview:priceLabel];
        [view addSubview:itemImage];
    }
    
    //set item label
    //remember to always set any properties of your carousel item
    //views outside of the `if (view == nil) {...}` check otherwise
    //you'll get weird issues with carousel item content appearing
    //in the wrong place in the carousel
    
    return view;
}

- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index
{

    NSLog(@"Tapped view number: %ld", (long)index);
}
- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    //customize carousel display
    switch (option)
    {
        case iCarouselOptionSpacing:
        {
            if (carousel == itemsView)
            {
                //add a bit of spacing between the item views
                return value*.43;
            }
        }
        default:
        {
            return value;
        }
    }
}

- (CGFloat)carouselItemWidth:(iCarousel *)carousel
{
    //usually this should be slightly wider than the item views
    return itemsView.frame.size.width + 20;
}
#pragma mark TABLE VIEW DELEGATES AND DATASOURCES



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [titleItemsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"passTableViewCell";
    
    UITableViewCell * tableViewCell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!tableViewCell) {
        tableViewCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    tableViewCell.textLabel.textAlignment = NSTextAlignmentCenter;
    tableViewCell.textLabel.text = titleItemsArray[indexPath.row];
    
    return tableViewCell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    switch (indexPath.row) {
        case 0:
            [self didMenuPressed];
            break;
        case 1:
            [self didCatalogPressed];
            break;
        case 2:
            [self showWorkRoom];
            break;
        case 3:
            
            break;
        case 4:
            [self showMyOrders];
            break;
        case 5:
            [self showMyCabinet];
            break;
        default:
            break;
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}
#pragma mark Other methods
- (void) startAnimationWithDirection:(BOOL)down {
    if (down) {
        [slidingView setFrame:CGRectMake(0, screenSize.height/2, slidingView.frame.size.width, slidingView.frame.size.height)];
        
    }else{
        [slidingView setFrame:CGRectMake(0, 0, slidingView.frame.size.width, slidingView.frame.size.height)];
    }
    animationDirection = !down;
}


#pragma mark ACTIONS

- (void) infoPressed{
    NSLog(@"info");
}

- (void) settingsPressed{
    [self.navigationController pushViewController:settingsVC animated:YES];
}
- (void) showWorkRoom{
    
    [self.navigationController pushViewController:workRoomVC animated:YES];
}
- (void) showMyOrders {
    [self.navigationController pushViewController:myOrderVC animated:YES];
}
- (void) showMyCabinet{
    
    if (!authVC.isSignedIn) {
        [self.navigationController pushViewController:authVC animated:YES];
    }else{
    
    }
    
    
}

#pragma mark MainMenuView Delegates

-(void)didMenuPressed {
    
    [UIView animateWithDuration:0.6f
                          delay:0.1f
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [self startAnimationWithDirection:animationDirection];
                     }
                     completion:^(BOOL finished){

                     }
     ];

}
-(void)didCreatePressed{
    
    [self showWorkRoom];

}
-(void)didCatalogPressed{
    
    CatalogViewController *catalogViewController = [[CatalogViewController alloc] init];

    [self.navigationController pushViewController:catalogViewController animated:YES];

}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
