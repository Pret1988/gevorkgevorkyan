//
//  BeltDetailViewController.m
//  GevorkGevorkyan
//
//  Created by Garnik Ghazaryan on 11/18/14.
//  Copyright (c) 2014 Garnik Ghazaryan. All rights reserved.
//

#import "BeltDetailViewController.h"
#import "Utils/Utils.h"
#import "MZFormSheetController.h"
#import "AKPickerView.h"

@interface BeltDetailViewController () <AKPickerViewDelegate>

@property (nonatomic, strong) AKPickerView *pickerView;
@property (nonatomic, strong) NSArray *filterTitles;

@end

@implementation BeltDetailViewController {
    
    UILabel *titleLabel;
    UILabel *priceLabel;
    
    UIImageView *beltImageView;
    UIImageView *pickedNumberBgImage;
    UIImageView *dotAndStickImageView;
    
    UIButton *closeButton;
    UIButton *helpButton;
    UIButton *plusButton;
    UIButton *minusButton;
    UIButton *addButton;
    
    UITextView *descriptionTextView;
    
    CGSize screenSize;
    CGSize textSize;
    NSUInteger currentNumber;

}
- (id)init
{
    self = [super init];
    if (self) {
        self.view.backgroundColor = [UIColor whiteColor];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    screenSize = CGSizeMake(300, 400);
    [self uiConfigurations];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{



}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.Хочу три таких ремешка вокруг верхней части сапога с расстоянием друг от друга, примерно, в 5 см. Только без пряжки, а на липучках.
}
- (void) uiConfigurations{
    
    [Utils setViewToAddOn:self.view];
    textSize = [Utils getTextBoundsSize:@"Ремешок из кожи" font:[UIFont systemFontOfSize:17]];
    titleLabel = [Utils createLabelWithRect:CGRectMake(screenSize.width/2 - textSize.width/2, 0, textSize.width, 40) title:@"Ремешок из кожи"];
    closeButton = [Utils createButtonInRect:CGRectMake(5, 5, 20, 20) title:@"" target:self action:@selector(closeView) backGroundImage:[UIImage imageNamed:@"beltViewClose.png"] highlightImage:nil];
    helpButton = [Utils createButtonInRect:CGRectMake(screenSize.width - 30, 5, 20, 20) title:@"" target:self action:@selector(showHelp) backGroundImage:[UIImage imageNamed:@"beltViewHelpButtonImage.png"] highlightImage:nil];
    beltImageView = [Utils createImageViewWithRect:CGRectMake(screenSize.width/2 - 50, titleLabel.frame.origin.y + titleLabel.frame.size.height + 10, 100, 100) image:[UIImage imageNamed:@"beltImage.png"]];
    priceLabel = [Utils createLabelWithRect:CGRectMake(screenSize.width/2 - 75, beltImageView.frame.origin.y + beltImageView.frame.size.height + 5, 150, 40) title:@"900 P"];
    dotAndStickImageView = [Utils createImageViewWithRect:CGRectMake(0, priceLabel.frame.origin.y + priceLabel.frame.size.height + 5, screenSize.width, 17) image:[UIImage imageNamed:@"dotAndStick.png"]];
    minusButton = [Utils createButtonInRect:CGRectMake(20, dotAndStickImageView.frame.origin.y + dotAndStickImageView.frame.size.height + 13 , 20, 5) title:@"" target:self action:@selector(decreaseNumber) backGroundImage:[UIImage imageNamed:@"minusStepper.png"] highlightImage:nil];
    plusButton = [Utils createButtonInRect:CGRectMake(screenSize.width - 40, dotAndStickImageView.frame.origin.y + dotAndStickImageView.frame.size.height + 5 , 20, 20) title:@"" target:self action:@selector(increaseNumber) backGroundImage:[UIImage imageNamed:@"plusStepper.png"] highlightImage:nil];
    pickedNumberBgImage = [Utils createImageViewWithRect:CGRectMake(dotAndStickImageView.frame.size.width/2 - 17, dotAndStickImageView.frame.origin.y + dotAndStickImageView.frame.size.height, 33, 33) image:[UIImage imageNamed:@"pickedBumberBgImage.png"]];
    descriptionTextView = [Utils createTextViewWithRect:CGRectMake(5, plusButton.frame.origin.y + plusButton.frame.size.height + 15 , screenSize.width - 10, 80) contentText:@"Хочу три таких ремешка вокруг верхней части сапога с расстоянием друг от друга, примерно, в 5 см. Только без пряжки, а на липучках."];
    descriptionTextView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    descriptionTextView.layer.borderWidth = 1.f;
    addButton = [Utils createButtonInRect:CGRectMake(0, screenSize.height - 44, screenSize.width, 44) title:@"" target:self action:@selector(addButtonPressed) backGroundImage:[UIImage imageNamed:@"addButtonIMageBeltView.png"] highlightImage:nil];
    [self configurePickerView];
}

- (void) configurePickerView {
    
    self.pickerView = [[AKPickerView alloc] initWithFrame:CGRectMake(minusButton.frame.size.width + 27, minusButton.frame.origin.y - 55, screenSize.width - 75, 20)];
    self.pickerView.delegate = self;
//    self.pickerView.highlightedTextColor = rgba(253, 198, 128, 1);
    self.pickerView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.pickerView.backgroundColor = [UIColor clearColor];
//    self.pickerView.layer.borderWidth = 1.f;
//    self.pickerView.layer.borderColor = rgba(149, 149, 149, 1).CGColor;
    [self.pickerView selectItem:6 animated:NO];
    currentNumber = self.pickerView.selectedItem;
    
//    UIImageView *dotImage = [[UIImageView alloc] initWithFrame:CGRectMake(self.pickerView.frame.size.width/2 - 2, 5, 6, 6)];
//    dotImage.image = [UIImage imageNamed:@"selectedDot.png"];
//    [self.pickerView addSubview:dotImage];
    self.pickerView.font = [UIFont systemFontOfSize:16.0];
    self.pickerView.highlightedFont = [UIFont systemFontOfSize:18.0];
    self.pickerView.interitemSpacing = 20.0;
    self.pickerView.fisheyeFactor = 0.001;
    
    self.filterTitles = @[@"1",
                          @"2",
                          @"3",
                          @"4",
                          @"5",
                          @"6",
                          @"7",
                          @"8",
                          @"9",
                          @"10"];
    
    [self.pickerView reloadData];
    [self.view addSubview:self.pickerView];
}


- (void) closeView{
//    [self dismissViewControllerAnimated:YES completion:nil];
    [self mz_dismissFormSheetControllerAnimated:YES completionHandler:nil];
}
- (void) showHelp{
    
    
}
- (void) decreaseNumber{
    if (currentNumber > 0) {
        currentNumber--;
        [self.pickerView selectItem:currentNumber animated:YES];
    }
    
}
- (void) increaseNumber{
    if (currentNumber < [self.filterTitles count] - 1) {
        currentNumber++;
        [self.pickerView selectItem:currentNumber animated:YES];
    }
    
}
- (void) addButtonPressed{
    
    
}

#pragma mark AKPickerView Delegates

- (NSUInteger)numberOfItemsInPickerView:(AKPickerView *)pickerView
{
    return [self.filterTitles count];
}

- (NSString *)pickerView:(AKPickerView *)pickerView titleForItem:(NSInteger)item
{
    return self.filterTitles[item];
}

- (void)pickerView:(AKPickerView *)pickerView didSelectItem:(NSInteger)item
{
    NSLog(@"%@", self.filterTitles[item]);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
