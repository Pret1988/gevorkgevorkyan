//
//  WorkroomViewController.m
//  GevorkGevorkyan
//
//  Created by Garnik Ghazaryan on 10/29/14.
//  Copyright (c) 2014 Garnik Ghazaryan. All rights reserved.
//

#import "WorkroomViewController.h"
#import "Utils.h"
#import "AKPickerView.h"
#import <QuartzCore/QuartzCore.h>
#import "ShoeEditorViewController.h"

@interface WorkroomViewController ()<AKPickerViewDelegate>

@property (nonatomic, strong) AKPickerView *pickerView;
@property (nonatomic, strong) NSArray *filterTitles;


@end

@implementation WorkroomViewController{

    CGSize screenSize;
    UISwitch *femMaleSwitch;
    UILabel *femaleLabel;
    UILabel *maleLabel;
    UIImageView *editingBgView;

    UIImageView *shoeTypeView;
    UIButton *createNewShoeButton;
    
    UIButton *leftArrowButton;
    UIButton *rightArrowButton;
    
}

- (void)viewDidLoad {
    

    self.view = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    self.view.backgroundColor = rgba(250, 249, 245, 1);
    self.title = @"МАСТЕРСКАЯ";
    self.navigationController.navigationBar.backgroundColor = [UIColor whiteColor];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithTitle:@"НАЗАД" style:UIBarButtonItemStyleBordered target:self action:@selector(goBack)];
    [leftItem  setTitleTextAttributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:14] } forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithTitle:@"ЭСКИЗЫ" style:UIBarButtonItemStyleBordered target:self action:@selector(showEditor)];
    [rightItem  setTitleTextAttributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:14.0] } forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = rightItem;
    self.navigationController.navigationBar.tintColor = [UIColor grayColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor whiteColor];
    
    screenSize = [Utils getScreenBounds];    // Do any additional setup after loading the view.
    [self uiConfigurations];
    [super viewDidLoad];
}
-(void)viewWillAppear:(BOOL)animated{
    [self.view addSubview:self.pickerView];
}
-(void)viewWillDisappear:(BOOL)animated{
    [self.pickerView removeFromSuperview];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) uiConfigurations{
    
    [Utils setViewToAddOn:self.view];
    editingBgView = [Utils createImageViewWithRect:CGRectMake(0, 44, screenSize.width, 103) image:[UIImage imageNamed:@"shoeMobImage.png"]];
    femMaleSwitch = [Utils createSwitchWithRect:CGRectMake(screenSize.width/2 - 25, editingBgView.frame.origin.y + editingBgView.frame.size.height + 5, 51, 51) target:self action:@selector(feMailSwitch)];

    femaleLabel = [Utils createLabelWithRect:CGRectMake(femMaleSwitch.frame.origin.x - 30, femMaleSwitch.frame.origin.y, 30, 30) title:@"Ж"];
    maleLabel = [Utils createLabelWithRect:CGRectMake(femMaleSwitch.frame.origin.x + femMaleSwitch.frame.size.width + 10, femMaleSwitch.frame.origin.y, 30, 30) title:@"М"];
    
    
    createNewShoeButton = [Utils createButtonInRect:CGRectMake(0, screenSize.height - 50, screenSize.width, 50) title:@"" target:self action:@selector(createNewShoePressed) backGroundImage:[UIImage imageNamed:@"createNewShoeButtonImage.png"] highlightImage:nil];
    [self configurePickerView];
    
    shoeTypeView = [Utils createImageViewWithRect:CGRectMake(screenSize.width/2 - 52,self.pickerView.frame.origin.y + self.pickerView.frame.size.height + (createNewShoeButton.frame.origin.y - (self.pickerView.frame.origin.y + self.pickerView.frame.size.height))/2 - 72, 104, 145) image:[UIImage imageNamed:@"rawShoeimageTransparent.png"]];
    
    
    leftArrowButton = [Utils createButtonInRect:CGRectMake(10, shoeTypeView.frame.origin.y + shoeTypeView.frame.size.height/2 - 15, 30, 30) title:@"" target:self action:@selector(selectLeftItem) backGroundImage:[UIImage imageNamed:@"leftSideArrow.png"] highlightImage:nil];
    
    rightArrowButton = [Utils createButtonInRect:CGRectMake(screenSize.width - 40, shoeTypeView.frame.origin.y + shoeTypeView.frame.size.height/2 - 15, 30, 30) title:@"" target:self action:@selector(selectRightItem) backGroundImage:[UIImage imageNamed:@"rightSideArrow.png"] highlightImage:nil];

}

- (void) configurePickerView {
    
    self.pickerView = [[AKPickerView alloc] initWithFrame:CGRectMake(-100, femMaleSwitch.frame.origin.y + femMaleSwitch.frame.size.height + 10, screenSize.width + 200, 60)];
    self.pickerView.delegate = self;
    self.pickerView.highlightedTextColor = rgba(253, 198, 128, 1);
    self.pickerView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.pickerView.backgroundColor = rgba(250, 250, 245, 1);
    self.pickerView.layer.borderWidth = 1.f;
    self.pickerView.layer.borderColor = rgba(149, 149, 149, 1).CGColor;
    [self.pickerView selectItem:2 animated:NO];
    
    UIImageView *dotImage = [[UIImageView alloc] initWithFrame:CGRectMake(self.pickerView.frame.size.width/2 - 2, 5, 6, 6)];
    dotImage.image = [UIImage imageNamed:@"selectedDot.png"];
    [self.pickerView addSubview:dotImage];
    self.pickerView.font = [UIFont systemFontOfSize:16.0];
    self.pickerView.highlightedFont = [UIFont systemFontOfSize:18.0];
    self.pickerView.interitemSpacing = 20.0;
    self.pickerView.fisheyeFactor = 0;
    
    self.filterTitles = @[@"САПОГИ",
                          @"ТУФЛИ",
                          @"НОВИНКИ",
                          @"БАЛЕТКИ",
                          @"БОТИНКИ",
                          @"Chiba",
                          @"Hyogo",
                          @"Hokkaido",
                          @"Fukuoka",
                          @"Shizuoka"];
    
    [self.pickerView reloadData];
}
#pragma mark AKPickerView Delegates

- (NSUInteger)numberOfItemsInPickerView:(AKPickerView *)pickerView
{
    return [self.filterTitles count];
}

- (NSString *)pickerView:(AKPickerView *)pickerView titleForItem:(NSInteger)item
{
    return self.filterTitles[item];
}

- (void)pickerView:(AKPickerView *)pickerView didSelectItem:(NSInteger)item
{
    NSLog(@"%@", self.filterTitles[item]);
}



#pragma mark Actions
- (void) goBack{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void) createNewShoePressed{
    ShoeEditorViewController *shoeEditor = [[ShoeEditorViewController alloc] init];
    [self.navigationController pushViewController:shoeEditor animated:YES];
}
- (void) showEditor{
    [self createNewShoePressed];
}
- (void) feMailSwitch{
    
    
}
- (void) selectLeftItem{
    
    if (self.pickerView.selectedItem > 0) {
        [self.pickerView selectItem:self.pickerView.selectedItem - 1 animated:YES];
    }
}
- (void) selectRightItem{
    if (self.pickerView.selectedItem < [self.filterTitles count] - 1) {
        [self.pickerView selectItem:self.pickerView.selectedItem + 1 animated:YES];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
