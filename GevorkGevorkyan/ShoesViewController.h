//
//  ShoesViewController.h
//  GevorkGevorkyan
//
//  Created by Garnik Ghazaryan on 10/26/14.
//  Copyright (c) 2014 Garnik Ghazaryan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShoesViewController : UIViewController<UIScrollViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate>

@end
