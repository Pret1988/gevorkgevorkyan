//
//  MyOrdersViewController.m
//  GevorkGevorkyan
//
//  Created by Garnik Ghazaryan on 12/1/14.
//  Copyright (c) 2014 Garnik Ghazaryan. All rights reserved.
//

#import "MyOrdersViewController.h"
#import "Utils.h"
#import "MyOrdersTableViewCell.h"
#import "ReadyShoeViewController.h"

#import "CheckOutViewController.h"
@interface MyOrdersViewController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation MyOrdersViewController {
    UITableView *orderTableView;
    CGSize screenSize;
    NSMutableArray *currentOrders;
    NSMutableArray *orderHistory;
    
    ReadyShoeViewController *readyShoeVC;
    CheckOutViewController *checkOutVC;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    self.view.backgroundColor = rgba(245, 243, 236, 1);
    self.title = @"МОИ ЗАКАЗЫ";
    self.navigationController.navigationBar.backgroundColor = [UIColor whiteColor];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithTitle:@"МЕНЮ" style:UIBarButtonItemStyleBordered target:self action:@selector(goBack)];
    [leftItem  setTitleTextAttributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:14]} forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem = leftItem;
    // Do any additional setup after loading the view.
    self.navigationController.navigationBar.tintColor = [UIColor grayColor];
    screenSize = [Utils getScreenBounds];
    [self uiConfigurations];
    
    readyShoeVC = [[ReadyShoeViewController alloc] init];
    checkOutVC = [[CheckOutViewController alloc] init];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void) uiConfigurations{
    
    orderTableView = [[UITableView alloc] initWithFrame:self.view.frame style:UITableViewStyleGrouped];
    orderTableView.delegate = self;
    orderTableView.dataSource = self;
    orderTableView.backgroundColor = [UIColor clearColor];
//    orderTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:orderTableView];
    
    currentOrders = [NSMutableArray arrayWithArray:@[@"PUMPS IN REDDISH PURPLE METAL PATENT LEATHER",@"Женские ТУФЛИ по заказу"]];
    orderHistory = [NSMutableArray arrayWithArray:@[@"PUMPS IN REDDISH PURPLE METAL PATENT LEATHER"]];
}

#pragma mark Tableview delegate and datasourse

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 2;
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section

{
    if (section == 0) {
        return [currentOrders count];
    }else if (section == 1){
        return [orderHistory count];
    }else return 0;
    
}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    if (section == 0) {
        return @"ФОРМА1";
    }else if (section == 1){
        return @"МАТЕРИАЛ1";
    }else return 0;
    
}
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 120;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 120;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *sectionView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)];
    [sectionView setBackgroundColor:[UIColor clearColor]];
    
    UILabel *tempLabel=[[UILabel alloc]initWithFrame:CGRectMake(8,15, 300, 30)];
    tempLabel.backgroundColor=[UIColor clearColor];
    tempLabel.textColor = rgba(78, 60, 54, 1); //here you can change the text color of header.
    
    if (section == 0) {
        tempLabel.text = @"ТЕКУЩИЕ ЗАКАЗЫ";
        tempLabel.frame = CGRectMake(15, 22, 300, 30);
    }else if (section == 1){
        tempLabel.text = @"ИСТОРИЯ ЗАКАЗОВ";
        tempLabel.frame = CGRectMake(15, 5, 300, 30);
    }
    
    [sectionView addSubview:tempLabel];
    
    
    return sectionView;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    static NSString *cellIdentifier = @"myOrdersTableViewCell";
    

    MyOrdersTableViewCell *cell = (MyOrdersTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MyOrdersTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    if (indexPath.section == 0) {
        cell.orderNumberLabel.text = @"№ 57024 | 24.02.2014";
        cell.orderItemNameLabel.text = [currentOrders objectAtIndex:indexPath.row];
        cell.orderStatusLabel.text = @"Статус: Подготовка материалов";
        cell.orderPriceLabel.text = @"8 410 Р";
        if (indexPath.row == 1) {
            cell.orderCellShoeImage.image = [UIImage imageNamed:@"customShoe.png"];
        }
    }else if (indexPath.section == 1){
        cell.orderNumberLabel.text = @"№ 57024 | 24.02.2014";
        cell.orderItemNameLabel.text = [orderHistory objectAtIndex:indexPath.row];
        cell.orderStatusLabel.text = @"Статус: Обувь готова";
        cell.orderPriceLabel.text = @"8 410 Р";
    }

    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{
    if (indexPath.section == 0) {
        [self.navigationController pushViewController:checkOutVC animated:YES];
        [checkOutVC showStatus];
    }
}
#pragma mark Actions

- (void) goBack{
    [self.navigationController popViewControllerAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
