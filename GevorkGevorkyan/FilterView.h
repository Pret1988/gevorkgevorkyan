//
//  FilterViewController.h
//  GevorkGevorkyan
//
//  Created by Garnik Ghazaryan on 10/23/14.
//  Copyright (c) 2014 Garnik Ghazaryan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SAMenuDropDown.h"

@interface FilterView : UIView<SAMenuDropDownDelegate>

@property (nonatomic, strong) SAMenuDropDown *menuDrop;

@end
