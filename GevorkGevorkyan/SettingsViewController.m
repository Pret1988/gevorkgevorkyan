//
//  SettingsViewController.m
//  GevorkGevorkyan
//
//  Created by Garnik Ghazaryan on 12/2/14.
//  Copyright (c) 2014 Garnik Ghazaryan. All rights reserved.
//

#import "SettingsViewController.h"
#import "Utils/Utils.h"
#import "KingBirdStudioViewController.h"

@interface SettingsViewController ()<UITableViewDelegate,UITableViewDataSource>

@end

@implementation SettingsViewController{

    UITableView *settingTableView;
    UISwitch *statusNoteSwitch;
    UISwitch *newsNoteSwitch;
    
    
    CGSize screenSize;
    NSArray *iconsArray;
    KingBirdStudioViewController *kingBirdVC;

}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    self.view.backgroundColor = rgba(245, 243, 236, 1);
    self.title = @"НАСТРОЙКИ";
    self.navigationController.navigationBar.backgroundColor = [UIColor whiteColor];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithTitle:@"МЕНЮ" style:UIBarButtonItemStyleBordered target:self action:@selector(goBack)];
    [leftItem  setTitleTextAttributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:14]} forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem = leftItem;
    // Do any additional setup after loading the view.
    self.navigationController.navigationBar.tintColor = [UIColor grayColor];
    screenSize = [Utils getScreenBounds];
    [self uiConfigurations];

    kingBirdVC = [[KingBirdStudioViewController alloc] init];
    // Do any additional setup after loading the view.
}

- (void) uiConfigurations{
    
    settingTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    settingTableView.delegate = self;
    settingTableView.dataSource = self;
    settingTableView.backgroundColor = [UIColor clearColor];
    //    orderTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:settingTableView];
    
    
    statusNoteSwitch = [Utils createSwitchWithRect:CGRectMake(screenSize.width - 58, 8, 40, 35) target:self action:@selector(statusNotiSwitcher)];
    newsNoteSwitch  = [Utils createSwitchWithRect:CGRectMake(screenSize.width - 58, 8, 40, 35) target:self action:@selector(newsNotifSwitcher)];
    
    iconsArray = [NSArray arrayWithObjects:[UIImage imageNamed:@"vkIconImage.png"],[UIImage imageNamed:@"fbIconImage.png"],[UIImage imageNamed:@"instagramIconImage.png"], nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark Tableview delegate and datasourse

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 3;
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section

{
    if (section == 0) {
        return 2;
    }else if (section == 1){
        return 3;
    }else if (section == 2){
        return 1;
    }else return 0;
    
}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    if (section == 0) {
        return @"ФОРМА1";
    }else if (section == 1){
        return @"МАТЕРИАЛ1";
    }else if (section == 2){
        return @"МАТЕРИАЛ1";
    }
    return 0;
    
}
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *sectionView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)];
    [sectionView setBackgroundColor:[UIColor clearColor]];
    
    UILabel *tempLabel=[[UILabel alloc]initWithFrame:CGRectMake(8,15, 300, 30)];
    tempLabel.backgroundColor=[UIColor clearColor];
    tempLabel.textColor = rgba(231, 198, 128, 1); //here you can change the text color of header.
    
    if (section == 0) {
        tempLabel.text = @"УВЕДОМЛЕНИЯ:";
        tempLabel.frame = CGRectMake(15, 22, 300, 30);
    }else if (section == 1){
        tempLabel.text = @"СОЦИАЛЬНЫЕ СЕТИ:";
        tempLabel.frame = CGRectMake(15, 5, 300, 30);
    }else if (section == 2){
        tempLabel.text = @"Разработчик:";
        tempLabel.frame = CGRectMake(15, 5, 300, 30);
    }

    
    [sectionView addSubview:tempLabel];
    
    
    return sectionView;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    static NSString *cellIdentifier = @"cellIdentifier";
    
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }

    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            cell.textLabel.text = @"Статус заказа";
            [cell addSubview:statusNoteSwitch];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }else if (indexPath.row == 1){
            cell.textLabel.text = @"Новости и акции";
            [cell addSubview:newsNoteSwitch];
        }
    }else if (indexPath.section == 1){
        UIImageView *socialImageView = [[UIImageView alloc] initWithFrame:CGRectMake(8, 2, 140, 40)];
        socialImageView.image = [iconsArray objectAtIndex:indexPath.row];
        [cell addSubview:socialImageView];
    }else if (indexPath.section == 2){
        UIImageView *kingBird = [[UIImageView alloc] initWithFrame:CGRectMake(8, 2, 185, 40)];
        kingBird.image = [UIImage imageNamed:@"kingBirdstudio.png"];
        [cell addSubview:kingBird];
    }
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{
    if (indexPath.section == 0) {
        
    }else if (indexPath.section == 1){
        if (indexPath.row == 0) {
            
        }else if (indexPath.row == 1){
            [self postOnFacebook];
        }else if (indexPath.row == 2){
            [self openInstagram];
        }
    }else if (indexPath.section == 2){
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
#pragma mark Actions

- (void) goBack{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void) statusNotiSwitcher{
    
    NSLog(@"1");
    
}
- (void) newsNotifSwitcher{
    NSLog(@"2");
    
}
- (void) postOnFacebook{
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) //check if Facebook Account is linked
    {
        slComposerSheet = [[SLComposeViewController alloc] init]; //initiate the Social Controller
        slComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook]; //Tell him with what social plattform to use it, e.g. facebook or twitter
        [slComposerSheet setInitialText:[NSString stringWithFormat:@"Test:%@",slComposerSheet.serviceType]]; //the message you want to post
        [slComposerSheet addImage:[UIImage imageNamed:@"reddishShoesCropped.png"]]; //an image you could post
        //for more instance methodes, go here:https://developer.apple.com/library/ios/#documentation/NetworkingInternet/Reference/SLComposeViewController_Class/Reference/Reference.html#//apple_ref/doc/uid/TP40012205
        [self presentViewController:slComposerSheet animated:YES completion:nil];
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Your Facebook account is not set up." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
}
- (void) openInstagram{
    NSURL *instagramURL = [NSURL URLWithString:@"instagram://location?id=1"];
    if ([[UIApplication sharedApplication] canOpenURL:instagramURL]) {
        [[UIApplication sharedApplication] openURL:instagramURL];
    }
}
- (void) kingBird{
    
    [self.navigationController pushViewController:kingBirdVC animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
