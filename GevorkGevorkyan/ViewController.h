//
//  ViewController.h
//  GevorkGevorkyan
//
//  Created by Garnik Ghazaryan on 10/14/14.
//  Copyright (c) 2014 Garnik Ghazaryan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITextFieldDelegate>

@property (nonatomic) BOOL isSignedIn;
@end

