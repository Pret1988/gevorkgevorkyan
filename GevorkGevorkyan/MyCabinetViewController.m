//
//  MyCabinetViewController.m
//  GevorkGevorkyan
//
//  Created by Garnik Ghazaryan on 12/3/14.
//  Copyright (c) 2014 Garnik Ghazaryan. All rights reserved.
//

#import "MyCabinetViewController.h"
#import "Utils.h"
#import "MyOrdersViewController.h"
#import "MySketchesViewController.h"
#import "CertificatesViewController.h"
#import "NewUserViewController.h"
#import "ViewController.h"
#import "EnterFootSizeViewController.h"

@interface MyCabinetViewController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation MyCabinetViewController{

    CGSize screenSize;
    UIImageView *profilePicImageView;
    UILabel *yourBalanceTitleLabel;
    UILabel *balanceLabel;
    UILabel *addLabel;
    UIButton *addButton;
    UIImageView *dotImageView;
    UITableView *bodyTableView;

    NSArray *titleItemsArray;
    int numberOfOrders;
    int numberOfSketches;
    
    MyOrdersViewController *myOrdersVC;
    MySketchesViewController *mySketchesVC;
    CertificatesViewController *certificateVC;
    NewUserViewController *newUserVC;
    ViewController *authVC;
    EnterFootSizeViewController *feetSizesVC;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    self.view.backgroundColor = rgba(250, 249, 245, 1);
    self.title = @"Руслан Сапожников";
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithTitle:@"МЕНЮ" style:UIBarButtonItemStyleBordered target:self action:@selector(goBack)];
    [leftItem  setTitleTextAttributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:14] } forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithTitle:@"ВЫЙТИ" style:UIBarButtonItemStyleBordered target:self action:@selector(logOut)];
    [rightItem  setTitleTextAttributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:14.0] } forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = rightItem;
    self.navigationController.navigationBar.tintColor = [UIColor grayColor];
    
    screenSize = [Utils getScreenBounds];
    numberOfOrders = 2;
    numberOfSketches = 5;
    titleItemsArray = @[[NSString stringWithFormat:@"МОИ ЗАКАЗЫ: %d",numberOfOrders],[NSString stringWithFormat:@"МОИ ЗАКАЗЫ: %d",numberOfSketches],@"СЕРТИФИКАТЫ",@"РАЗМЕРЫ НОГ",@"ИЗМЕНИТЬ ПРОФИЛЬ"];
    [self uiConfigurations];
    
    myOrdersVC = [[MyOrdersViewController alloc] init];
    mySketchesVC = [[MySketchesViewController alloc] init];
    certificateVC = [[CertificatesViewController alloc] init];
    newUserVC = [[NewUserViewController alloc] init];
    authVC = [[ViewController alloc] init];
    feetSizesVC = [[EnterFootSizeViewController alloc] init];
}

- (void) uiConfigurations{
    
    [Utils setViewToAddOn:self.view];
    profilePicImageView = [Utils createImageViewWithRect:CGRectMake(0, 0, screenSize.width, 310) image:[UIImage imageNamed:@"tmpProfilePic.png"]];
    balanceLabel = [Utils createLabelWithRect:CGRectMake(10, profilePicImageView.bounds.size.height - 50, 100, 40) title:@"12 000 P"];
    balanceLabel.font = [UIFont fontWithName:@"GothamPro" size:20];
    balanceLabel.textColor = rgba(231, 198, 128, 1);
    
    addLabel = [Utils createLabelWithRect:CGRectMake(profilePicImageView.bounds.size.width - 150, profilePicImageView.bounds.size.height - 40, 100, 30) title:@"ПОПОЛНИТЬ"];
    addLabel.font = [UIFont fontWithName:@"GothamPro" size:14];
    
    addButton = [Utils createButtonInRect:CGRectMake(profilePicImageView.bounds.size.width - 40, balanceLabel.frame.origin.y, 30, 30) title:@"" target:self action:@selector(addBalancePressed) backGroundImage:[UIImage imageNamed:@"addButtonImage.png"] highlightImage:nil];
    dotImageView = [Utils createImageViewWithRect:CGRectMake(5, balanceLabel.frame.origin.y - 10, screenSize.width - 10, 2) image:[UIImage imageNamed:@"dotImage.png"]];
    
    yourBalanceTitleLabel = [Utils createLabelWithRect:CGRectMake(10, dotImageView.frame.origin.y - 40, 70, 30) title:@"ВАШ БАЛАНС:"];
    yourBalanceTitleLabel.font = [UIFont fontWithName:@"GothamPro" size:12];
    
    bodyTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, profilePicImageView.frame.size.height - 1, screenSize.width, screenSize.height - profilePicImageView.frame.origin.y + 44) style:UITableViewStyleGrouped];
    bodyTableView.delegate = self;
    bodyTableView.dataSource = self;
    bodyTableView.backgroundColor = [UIColor clearColor];
    //    orderTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:bodyTableView];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark TABLE VIEW DELEGATES AND DATASOURCES

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section{
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [titleItemsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"passTableViewCell";
    
    UITableViewCell * tableViewCell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!tableViewCell) {
        tableViewCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    tableViewCell.textLabel.textAlignment = NSTextAlignmentCenter;
    tableViewCell.textLabel.text = titleItemsArray[indexPath.row];
    
    return tableViewCell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    switch (indexPath.row) {
        case 0:
            [self showMyOrders];
            break;
        case 1:
            [self showMySketches];
            break;
        case 2:
            [self showCertificates];
            break;
        case 3:
            [self showFeetSizes];
            break;
        case 4:
            [self showEditProfile];
            break;
        default:
            break;
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

#pragma mark Actions


- (void) goBack{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void) logOut{
    [self.navigationController popToViewController:authVC animated:YES];
}
- (void) addBalancePressed{
    
    
}
- (void) showMyOrders {
    [self.navigationController pushViewController:myOrdersVC animated:YES];
}
- (void) showMySketches {
    [self.navigationController pushViewController:mySketchesVC animated:YES];
}
- (void) showCertificates {
    [self.navigationController pushViewController:certificateVC animated:YES];
}
- (void) showFeetSizes {
    [self.navigationController pushViewController:feetSizesVC animated:YES];
}

- (void) showEditProfile {
    [self.navigationController pushViewController:newUserVC animated:YES];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
