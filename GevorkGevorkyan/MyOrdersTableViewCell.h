//
//  MyrOrdersTableViewCell.h
//  GevorkGevorkyan
//
//  Created by Garnik Ghazaryan on 12/2/14.
//  Copyright (c) 2014 Garnik Ghazaryan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyOrdersTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *orderCellShoeImage;
@property (strong, nonatomic) IBOutlet UILabel *orderNumberLabel;
@property (strong, nonatomic) IBOutlet UILabel *orderItemNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *orderStatusLabel;
@property (strong, nonatomic) IBOutlet UILabel *orderPriceLabel;

@end
