//
//  CatalogViewController.h
//  GevorkGevorkyan
//
//  Created by Garnik Ghazaryan on 10/20/14.
//  Copyright (c) 2014 Garnik Ghazaryan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CatalogViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
{
    UICollectionView *_collectionView;
}

@end
