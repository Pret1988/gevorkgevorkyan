//
//  EnterFootSizeViewController.m
//  GevorkGevorkyan
//
//  Created by Garnik Ghazaryan on 12/5/14.
//  Copyright (c) 2014 Garnik Ghazaryan. All rights reserved.
//

#import "EnterFootSizeViewController.h"
#import "Utils/Utils.h"
#import "EnterFootSizeTableViewCell.h"
#import "MZFormSheetController.h"
#import "FootSizeAdjustViewController.h"
@interface EnterFootSizeViewController ()<UITableViewDataSource,UITableViewDelegate,FootSizeAdjustDelegate>

@end

@implementation EnterFootSizeViewController{

    CGSize screenSize;
    NSArray *titleItemsArray;
    NSMutableArray *oldTitlesArray;
    
    UIButton *leftFootButton;
    UIButton *rightFootButton;
    
    UILabel *leftLabel;
    UILabel *rightLabel;
    
    NSString *sizeText;
    
    UITableView *footParametersTableView;
    
    FootSizeAdjustViewController *footSizeAdjuster;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    self.view.backgroundColor = rgba(250, 249, 245, 1);
    self.title = @"РАЗМЕРЫ НОГ";
    self.navigationController.navigationBar.backgroundColor = [UIColor whiteColor];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithTitle:@"НАЗАД" style:UIBarButtonItemStyleBordered target:self action:@selector(goBack)];
    [leftItem  setTitleTextAttributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:14] } forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithTitle:@"ГОТОВО" style:UIBarButtonItemStyleBordered target:self action:@selector(logOut)];
    [rightItem  setTitleTextAttributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:14.0] } forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = rightItem;
    self.navigationController.navigationBar.tintColor = [UIColor grayColor];
    
    screenSize = [Utils getScreenBounds];

    titleItemsArray = @[@"1. Длина стопы",@"2. Объем пальцев",@"3. Полнота ноги",@"4. Обхват ноги",@"5. Обхват у икры",@"6. Высота ноги"];
    oldTitlesArray = [NSMutableArray arrayWithArray:@[@"0 см",@"0 см",@"0 см",@"0 см",@"0 см",@"0 см"]];
    [self uiConfigurations];

    
}
- (void) uiConfigurations{
    [Utils setViewToAddOn:self.view];
    leftFootButton = [Utils createButtonInRect:CGRectMake(screenSize.width/2 - 74, 60, 64, 155) title:@"" target:self action:@selector(leftFootChosen) backGroundImage:[UIImage imageNamed:@"leftFootImage.png"] highlightImage:nil];
    rightFootButton = [Utils createButtonInRect:CGRectMake(screenSize.width/2 + 10, 60, 64, 155) title:@"" target:self action:@selector(rightFootChosen) backGroundImage:[UIImage imageNamed:@"rightFootImage.png"] highlightImage:nil];
   
    [leftFootButton setBackgroundImage:[UIImage imageNamed:@"leftFootImageSelected.png"] forState:UIControlStateSelected];
    [rightFootButton setBackgroundImage:[UIImage imageNamed:@"rightFootImageSelected.png"] forState:UIControlStateSelected];
    [leftFootButton setTintColor:[UIColor clearColor]];
    [rightFootButton setTintColor:[UIColor clearColor]];
    
    leftLabel = [Utils createLabelWithRect:CGRectMake(leftFootButton.frame.origin.x, leftFootButton.frame.origin.y + leftFootButton.frame.size.height + 10, 60, 30) title:@"ЛЕВАЯ"];
    rightLabel = [Utils createLabelWithRect:CGRectMake(rightFootButton.frame.origin.x, rightFootButton.frame.origin.y + rightFootButton.frame.size.height + 10, 80, 30) title:@"ПРАВАЯ"];
    
    sizeText = @"0 см";
    
    footParametersTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, leftLabel.frame.origin.y + leftLabel.frame.size.height, screenSize.width, screenSize.height - (leftLabel.frame.origin.y + 40)) style:UITableViewStyleGrouped];
    footParametersTableView.delegate = self;
    footParametersTableView.dataSource = self;
    footParametersTableView.backgroundColor = [UIColor clearColor];
    //    orderTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:footParametersTableView];
    
    footSizeAdjuster = [[FootSizeAdjustViewController alloc] initWithNibName:@"FootSizeAdjustViewController" bundle:[NSBundle mainBundle]];

    footSizeAdjuster.delegate = self;
    
}
- (void) showFormSheetView{
    
    
    footSizeAdjuster.preferredContentSize = CGSizeMake(300, 400);
    
    MZFormSheetController *formSheet = [[MZFormSheetController alloc] initWithViewController:footSizeAdjuster];
    formSheet.presentedFormSheetSize = CGSizeMake(300, 400);
    //    formSheet.transitionStyle = MZFormSheetTransitionStyleSlideFromTop;
    formSheet.shadowRadius = 2.0;
    formSheet.shadowOpacity = 0.3;
    formSheet.shouldDismissOnBackgroundViewTap = YES;
    formSheet.shouldCenterVertically = YES;
    formSheet.movementWhenKeyboardAppears = MZFormSheetWhenKeyboardAppearsCenterVertically;
    
    [self mz_presentFormSheetController:formSheet animated:YES completionHandler:nil];
    
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark TABLE VIEW DELEGATES AND DATASOURCES

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section{
    return 44;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 44;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [titleItemsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"enterFootSizeCellIdentifier";
    
    EnterFootSizeTableViewCell *cell = (EnterFootSizeTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"EnterFootSizeTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.titleLabel.text = [titleItemsArray objectAtIndex:indexPath.row];
    cell.sizeLabel.text = [oldTitlesArray objectAtIndex:indexPath.row];
    

    
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    switch (indexPath.row) {
        case 0:
//            [self showMyOrders];
            break;
        case 1:
//            [self showMySketches];
            break;
        case 2:
//            [self showCertificates];
            break;
        case 3:
            
            break;
        case 4:
//            [self showEditProfile];
            break;
        default:
            break;
    }
    footSizeAdjuster.selectedItemAtIndex = indexPath.row;
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    [self showFormSheetView];
}
-(void)didFinishSizeEditingWithText:(NSString *)inputSize{

    sizeText = inputSize;
    [oldTitlesArray replaceObjectAtIndex:footSizeAdjuster.selectedItemAtIndex withObject:sizeText];
    [footParametersTableView reloadData];
}
#pragma mark Actions


- (void) goBack{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void) leftFootChosen{
    
    [rightFootButton setSelected:NO];
    [leftFootButton setSelected:YES];

    
}
- (void) rightFootChosen{
    [rightFootButton setSelected:YES];
    [leftFootButton setSelected:NO];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
