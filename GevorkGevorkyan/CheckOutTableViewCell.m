//
//  CheckOutTableViewCell.m
//  GevorkGevorkyan
//
//  Created by Garnik Ghazaryan on 11/30/14.
//  Copyright (c) 2014 Garnik Ghazaryan. All rights reserved.
//

#import "CheckOutTableViewCell.h"
#import "Utils/Utils.h"

@implementation CheckOutTableViewCell

@synthesize leftLabel,rightLabel,dotImageView;

- (void)awakeFromNib {
    // Initialization code
    leftLabel = [[UILabel alloc] initWithFrame:CGRectMake(8, 4, 160, 21)];
    [leftLabel sizeToFit];
    [leftLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleRightMargin];
    leftLabel.backgroundColor = rgba(252, 252, 252, 1);
    leftLabel.textColor = rgba(153, 153, 153, 1);
    leftLabel.font = [UIFont fontWithName:@"GothamPro" size:14.0];
    [self addSubview:leftLabel];
    
    rightLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.frame.size.width - 65, 2, 60, 21)];
    rightLabel.backgroundColor = rgba(252, 252, 252, 1);
    rightLabel.textAlignment = NSTextAlignmentRight;
    rightLabel.textColor = rgba(153, 153, 153, 1);
    rightLabel.font = [UIFont fontWithName:@"GothamPro" size:14.0];
    [self addSubview:rightLabel];

    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
